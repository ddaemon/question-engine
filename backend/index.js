import { setup_sequelize }  from './database.js'
import { Command } from 'commander';
import { ethers } from 'ethers';
import express from 'express';
import { check_balance } from './helper_functions/check_balance.js'
import { verify_valid_transaction } from './helper_functions/verify_valid_transaction.js'
import { verify_cookie } from './helper_functions/verify_cookie.js'
import { verify_hash_and_signature } from './helper_functions/verify_hash_and_signature.js'
import { verify_mint_transaction } from './transactions/mint.js'
import { verify_burn_transaction } from './transactions/burn.js';
import { verify_transfer_transaction } from './transactions/transfer.js';
import { verify_remove_transaction } from './transactions/remove.js';
import { verify_use_faucet_transaction } from './transactions/use_faucet.js';
import { verify_edge_transaction } from './transactions/edge.js';
import { verify_meme_transaction } from './transactions/meme.js';
import { verify_group_create_transaction } from './transactions/group_create.js';
import { verify_group_manage_transaction } from './transactions/group_manage.js';
import { query_memes } from './helper_functions/query_memes.js';

const program = new Command();
program
  .option('-c, --connectionString <string>', 'SQL Connection String (sqlite default)')
  .option('-r, --reset', 'Delete what is in the database and regenerate schema.')
  .option('-p, --port <number>' , 'Set the port you want to run the app on. (3000 default)')
program.parse(process.argv);
const options = program.opts();
console.log(options)

// To get more context on db variable check start function in this file
let db = null


const app = express()
app.use(express.json())
// app.use(cookieParser())
let port = 4000
if ('port' in options){
    port = options.port
}

app.get('/api', (req, res) => {
    res.send('Hello World!')
})

// app.get('/api/test_cookie', (req, res) => {
//     if (!req.cookies.token) return res.status(401).send();
//     return res.json({
//         "cookies" : req.cookies,
//         "description" : "You have some cookies set"
//     })
// })

app.post('/api/check_access_token', async(req, res) => {
    if ( Object.keys(req.body).length != 3){
        res.json({
            "response_type" : "ERROR",
            "response_code" : "INVALID_JSON",
            "description"   : `Invalid number of keys /api/wield_persona, keys needed, ['data', 'hash', 'signature_of_hash']`
        })
        return true
    }
    let keys_needed = ['pseudonym', 'evm_address', 'access_token']
    if (  !keys_needed.every(key => Object.keys(req.body).includes(key))  ) {
        res.json({
            "response_type" : "ERROR",
            "response_code" : "INVALID_JSON",
            "description"   : `Invalid keys for /api/wield_persona, keys needed, ['data', 'hash', 'signature_of_hash']`
        })
        return true
    }
    let check_access_token = await db.models.AUTH_T.build({
        ACCESS_TOKEN : req.body.hash,
        PSEUDONYM    : req.body.pseudonym,
        PUBLIC_KEY   : req.body.evm_address
    })
    if( check_access_token == null){
        res.json({
            "response_type" : "ERROR",
            "response_code" : "INVALID_ACCESS_TOKEN",
            "description"   : `You may have to delete your cookies`
        })
        return true
    }
    res.json({
        "response_type" : "SUCCESS",
        "response_code" : "VALID_ACCESS_TOKEN",
        "description"   : `Continue being you`
    })
    return true

})

app.post('/api/wield_persona', async (req, res) => {
    if ( Object.keys(req.body).length != 3){
        res.json({
            "response_type" : "ERROR",
            "response_code" : "INVALID_JSON",
            "description"   : `Invalid number of keys /api/wield_persona, keys needed, ['data', 'hash', 'signature_of_hash']`
        })
        return true
    }
    let keys_needed = ['data', 'hash', 'signature_of_hash']
    if (  !keys_needed.every(key => Object.keys(req.body).includes(key))  ) {
        res.json({
            "response_type" : "ERROR",
            "response_code" : "INVALID_JSON",
            "description"   : `Invalid keys for /api/wield_persona, keys needed, ['data', 'hash', 'signature_of_hash']`
        })
        return true
    }
    let login_data = null
    try {
        login_data = JSON.parse(req.body.data)
    } catch {
        res.json({
            "response_type" : "ERROR",
            "response_code" : "INVALID_JSON",
            "description"   : `The JSON for key data was not valid`
        })
        return true
        
    }

    // raw hash and signature check
    let request_body_check = await verify_hash_and_signature(req.body) 
    if (request_body_check != true){
        res.json(request_body_check)
        return true
    } else {
        // Check if public key already registered
        let check_user_exists = await db.models.USER_T.findAll({
            where: {
                PUBLIC_KEY: ethers.utils.getAddress(login_data.signing_key)
            }
        })
        if (check_user_exists.length == 0){
            // Register user if not exists
            let USER_T_object = await db.models.USER_T.build({
                USER_SET_T_HASH : req.body.hash,
                PUBLIC_KEY : ethers.utils.getAddress(login_data.signing_key),
                PSEUDONYM : login_data.pseudonym,
                USER_SET_T_HASH : req.body.hash
            })
            let USER_T_result = await USER_T_object.save()
            let USER_ID = USER_T_result.dataValues.USER_ID
            let access_token_expiration_date = new Date()
            access_token_expiration_date.setDate(access_token_expiration_date.getDate() + 30);
            let auth_db_transaction = await db.models.AUTH_T.build(
                {
                    DATE_COOKIE_EXPIRED: access_token_expiration_date,
                    USER_ID    : USER_ID,
                    PUBLIC_KEY : ethers.utils.getAddress(login_data.signing_key),
                    PSEUDONYM  : login_data.pseudonym,
                    RAW_DATA   : req.body.data,
                    DATA       : login_data,
                    DATA_HASH  : req.body.hash,
                    DATE_DATA_SIGNED : login_data.unix_time_ms,
                    SIGNATURE_OF_DATA_HASH : req.body.signature_of_hash
                }
            )
            let result = await auth_db_transaction.save()
            res.json({
                "response_type"  : "SUCCESS",
                "response_code"  : "NOW_LOGGED_IN",
                "description"    : "Logging you in",
                "data"           : result
            })
            return true

        } else {
            if(check_user_exists[0].dataValues.PSEUDONYM != login_data.pseudonym){
                res.json({
                    "response_type"  : "ERROR",
                    "response_code"  : "INVALID_PSEUDONYM_PUBLIC_KEY_ALREADY_REGISTERED",
                    "description"    : `This public key already has a pseudonym, please log in with the correct pseudonym`
                })
                return true
            }
            // Log user in if exists
            let access_token_expiration_date = new Date()
            access_token_expiration_date.setDate(access_token_expiration_date.getDate() + 30);
            let auth_db_transaction = await db.models.AUTH_T.build(
                {
                    DATE_COOKIE_EXPIRED: access_token_expiration_date,
                    USER_ID    : check_user_exists[0].dataValues.USER_ID,
                    PUBLIC_KEY : ethers.utils.getAddress(login_data.signing_key),
                    PSEUDONYM  : login_data.pseudonym,
                    RAW_DATA   : req.body.data,
                    DATA       : login_data,
                    DATA_HASH  : req.body.hash,
                    DATE_DATA_SIGNED : login_data.unix_time_ms,
                    SIGNATURE_OF_DATA_HASH : req.body.signature_of_hash
                }
            )
            let result = await auth_db_transaction.save()
            console.log(result)
            res.json({
                "response_type"  : "SUCCESS",
                "response_code"  : "NOW_LOGGED_IN",
                "description"    : "Logging you in",
                "data"           : result
            })
            return true
        }
    }

})

app.post('/api/perform_transaction', async (req, res) => {

    // No cookie verification required, imagine this is a public blockchain except the data is managed separately

    let keys_needed = ["data", "hash", "signature_of_hash"]
    if ( !keys_needed.every(key => Object.keys(req.body).includes(key)) ){
        res.json({
            "response_type"  : "ERROR",
            "response_code"  : "MISSING_KEYS",
            "description" : "The JSON you sent us in your request does not have at minimum data, hash, and signature_of_hash keys"
        })
        return true
    }
    let request_transaction_data = null
    try {
        request_transaction_data = JSON.parse(req.body.data)
    } catch {
        res.json({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "The string of JSON you sent in your JSON does not parse"
        })
        return true
    }
    let request_body_check = await verify_hash_and_signature(req.body) 
    if (request_body_check != true){
        res.json(request_body_check)
        return true
    }
    // Check if user exists
    let get_user_uuid = await db.models.USER_T.findAll({
        where: {
            PSEUDONYM   : request_transaction_data.from_user_id_pseudonym,
            PUBLIC_KEY  : ethers.utils.getAddress(request_transaction_data.signing_key)
        }
    })
    if (get_user_uuid.length == 0){
        res.json({
            "response_type" : "ERROR",
            "response_code" : "INVALID_USER",
            "description"   : "Probably wrong pseudonym"
        })
        return true
    }
    // Check if transaction is valid
    let check_transaction = await verify_valid_transaction(request_transaction_data, get_user_uuid[0].USER_ID, db)
    if (check_transaction.response_type == "ERROR"){
        res.json(check_transaction)
        return true
    }
    if (request_transaction_data.transaction_code == "MINT"){
        let transaction_status = await verify_mint_transaction(
            req.body,
            request_transaction_data, 
            get_user_uuid[0].USER_ID, 
            db
        )
        res.json(transaction_status)
        return true
    }
    if (request_transaction_data.transaction_code == "BURN"){
        let transaction_status = await verify_burn_transaction(
            req.body,
            request_transaction_data, 
            get_user_uuid[0].USER_ID, 
            db
        )
        res.json(transaction_status)
        return true
    }
    if (request_transaction_data.transaction_code == "TRANSFER"){
        let transaction_status = await verify_transfer_transaction(
            req.body,
            request_transaction_data, 
            get_user_uuid[0].USER_ID, 
            db
        )
        res.json(transaction_status)
        return true
    }
    if (request_transaction_data.transaction_code == "REMOVE"){
        let transaction_status = await verify_remove_transaction(
            req.body,
            request_transaction_data, 
            get_user_uuid[0].USER_ID, 
            db
        )
        res.json(transaction_status)
        return true
    }
    if (request_transaction_data.transaction_code == "USE_FAUCET"){
        let transaction_status = await verify_use_faucet_transaction(
            req.body,
            request_transaction_data, 
            get_user_uuid[0].USER_ID, 
            db
        )
        res.json(transaction_status)
        return true
    }
    if (request_transaction_data.transaction_code == "MEME"){
        let transaction_status = await verify_meme_transaction(
            req.body,
            request_transaction_data, 
            get_user_uuid[0].USER_ID, 
            db
        )
        res.json(transaction_status)
        return true
    }
    if (request_transaction_data.transaction_code == "EDGE"){
        let transaction_status = await verify_edge_transaction(
            req.body,
            request_transaction_data, 
            get_user_uuid[0].USER_ID, 
            db
        )
        res.json(transaction_status)
        return true
    }
    if (request_transaction_data.transaction_code == "GROUP_CREATE"){
        let transaction_status = await verify_group_create_transaction(
            req.body,
            request_transaction_data, 
            get_user_uuid[0].USER_ID, 
            db
        )
        res.json(transaction_status)
        return true
    }
    if (request_transaction_data.transaction_code == "GROUP_MANAGE"){
        let transaction_status = await verify_group_manage_transaction(
            req.body,
            request_transaction_data, 
            get_user_uuid[0].USER_ID, 
            db
        )
        res.json(transaction_status)
        return true
    }
    res.json({
        "response_type" : "ERROR",
        "response_code" : "INVALID_TRANSACTION_CODE"
    })
})


app.post('/api/check_balance', async (req, res) => {

    // cookie logic
    if ( !("access_token" in req.body) ) return res.status(401).send();
    let cookie_status = false
    if (req.body.access_token != undefined) {
        cookie_status = await verify_cookie(req.body.access_token, db)
    }
    if(cookie_status == false){
        res.json({
            "response_type" : "ERROR",
            "response_code" : "UNABLE_TO_VALIDATE_ACCESS_TOKEN"
        })
        return true
    }

    res.json(await check_balance(req.body, db))
})


app.post('/api/query_memes', async (req, res) => {
    res.json(await query_memes(req.body, db))
    return true
})

async function start(){
    db = await setup_sequelize(options)
    app.listen(port, () => {
        console.log(`\nQuest(ion) Engine is listening on port ${port}`)
    })
}
start()