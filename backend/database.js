
// const { sequelize, DataTypes } = require('sequelize');
import { Sequelize, DataTypes } from 'sequelize';
import { Command } from 'commander';

const program = new Command();

program
  .option('-c, --connectionString <string>', 'SQL Connection String (sqlite default)')
  .option('-r, --reset', 'Delete what is in the database and regenerate schema.')
program.parse(process.argv);
const options = program.opts();
console.log(options)


export async function setup_sequelize(options){
  let db = {}
  db.models = {}
  db.sequelize = null
  if ("connectionString" in options){
    db.sequelize = new Sequelize(options.connectionString);
  }
  else {
    db.sequelize = new Sequelize({
      dialect: 'sqlite',
      storage: './database.sqlite'
    });
    // sequelize = new Sequelize('sqlite::memory:');
  }

  db.models.AUTH_T = db.sequelize.define('AUTH_T', {
    ACCESS_TOKEN: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    USER_ID: {
      type: DataTypes.UUID,
      allowNull: true
    },
    PSEUDONYM: {
      type: DataTypes.STRING,
      allowNull: true
    },
    PUBLIC_KEY: {
      type: DataTypes.STRING,
      allowNull: true
    },
    RAW_DATA: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    DATA: {
      type: DataTypes.JSON,
      allowNull: true
    },
    DATA_HASH: {
      type: DataTypes.STRING,
      allowNull: true
    },
    DATE_DATA_SIGNED: {
      type: DataTypes.DATE,
      allowNull: true
    },
    SIGNATURE_OF_DATA_HASH: {
      type: DataTypes.JSON,
      allowNull: true
    },
    DATE_COOKIE_EXPIRED: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    timestamps: true,
    tableName: 'AUTH_T'
  });

  db.models.USER_T = db.sequelize.define('USER_T', {
    USER_ID: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    USER_SET_T_HASH: {
      type: DataTypes.STRING
    },
    PUBLIC_KEY: {
      type: DataTypes.STRING
    },
    PUBLIC_KEY_UPDATE_T_HASH: {
      type: DataTypes.DATE
    },
    PSEUDONYM: {
      type: DataTypes.STRING
    },
    PSEUDONYM_UPDATE_T_HASH: {
      type: DataTypes.STRING
    }
  }, {
    timestamps: true,
    tableName: 'USER_T'
  });

  db.models.USER_PSEUDONYM_HIST_T = db.sequelize.define('USER_PSEUDONYM_HIST_T', {
    USER_ID: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    OLD_PSEUDONYM: {
      type: DataTypes.STRING,
      allowNull: false
    },
    SET_TRANSACTION_HASH: {
      type: DataTypes.STRING,
      allowNull: false
    },
    CHANGED_TRANSACTION_HASH: {
      type: DataTypes.DATE,
      allowNull: false
    },
    PUBLIC_KEY_AT_PSEUDONYM_CHANGE: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    timestamps: true,
    tableName: 'USER_PSEUDONYM_HIST_T'
  });

  db.models.MEMES_T = db.sequelize.define('MEMES_T', {
    T_HASH_OF_MEME: {
      type: DataTypes.STRING,
      allowNull: false
    },
    AUTHOR_USER_ID: {
      type: DataTypes.UUID,
      allowNull: false
    },
    RAW_MEME_CONTENT: {
      type: DataTypes.TEXT,
      allowNull: false 
    },
    HASH_OF_MEME: {
      type: DataTypes.STRING,
      allowNull: false
    },
    MEME_CONTENT: {
      type: DataTypes.JSON,
      allowNull: false
    },
    MEME_TYPE_CODE: {
      type: DataTypes.STRING,
      allowNull: false
    },
    MEME_PERMISSIONS: {
      type: DataTypes.STRING,
      allowNull: false
    },
    NUM_TRANSACTIONS_ATTACHED: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    RESPONSE_TO_MEME: {
      type: DataTypes.STRING, // MEMES_T.T_HASH_OF_MEME
      allowNull: true
    },
    ROOT_MEME: {
      type: DataTypes.STRING, // MEMES_T.T_HASH_OF_MEME
      allowNull: true
    },
    REPLACED_MEME_ID: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    timestamps: true,
    tableName: 'MEMES_T'
  });

  db.models.MEME_EDGES_T = db.sequelize.define('MEME_EDGES_T', {
    MEME_EDGE_ID: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    AUTHOR_USER_ID: {
      type: DataTypes.UUID,
      allowNull: false
    },
    SOURCE_MEME: {
      type: DataTypes.STRING,
      allowNull: true
    },
    REFERENCE_MEME: {
      type: DataTypes.STRING,
      allowNull: true
    },
    CONTEXT_DATA: {
      type: DataTypes.STRING,
      allowNull: true
    },
    MEME_EDGE_STATUS_CODE: {
      type: DataTypes.STRING,
      allowNull: false
    },
    TAG: {
      type: DataTypes.STRING,
      allowNull: true
    },
    NUM_TRANSACTIONS_ATTACHED: {
      type: DataTypes.STRING,
      allowNull: true
    },
    CONTEXT_DATA_HASH: {
      type: DataTypes.STRING,
      allowNull: true
    },
    REPLACED_MEME_EDGE_ID: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    timestamps: true,
    tableName: 'MEME_EDGES_T'
  });

  db.models.PUBLIC_KEY_ROTATION_T = db.sequelize.define('PUBLIC_KEY_ROTATION_T', {
    USER_ID: {
      type: DataTypes.UUID,
      allowNull: false
    },
    OLD_PUBLIC_KEY: {
      type: DataTypes.STRING,
      allowNull: false
    },
    NEW_PUBLIC_KEY: {
      type: DataTypes.STRING,
      allowNull: false
    },
    DATE_CHANGED: {
      type: DataTypes.DATE,
      allowNull: true
    },
    OLD_PKEY_TRANSACTION: {
      type: DataTypes.STRING,
      allowNull: false
    },
    NEW_PKEY_TRANSACTION: {
      type: DataTypes.STRING,
      allowNull: false
    },
    PSEUDONYM_AT_DATE_CHANGE: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    timestamps: true,
    tableName: 'PUBLIC_KEY_ROTATION_T'
  });


  db.models.TOKENS_T = db.sequelize.define('TOKENS_T', {
    USER_TOKEN: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    FAUCET_VALUE: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    FAUCET_FLOW: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    FAUCET_STATUS: {
      type: DataTypes.STRING,
      allowNull: true
    },
    FAUCET_STATUS_T_HASH: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    FAUCET_LAST_USE: {
      type: DataTypes.DATE,
      allowNull: true
    },
    TOKEN_STATUS: {
      type: DataTypes.STRING,
      allowNull: false
    },
    TOKEN_STATUS_T_HASH: {
      type: DataTypes.STRING,
      allowNull: true
    }
  },{
    timestamps: true,
    tableName: 'TOKENS_T'
  })


  db.models.BALANCES_T = db.sequelize.define('BALANCES_T', {
    USER_TOKEN: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    TOKEN_OWNER: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    TOKEN_AMOUNT: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    TOKEN_UNLOCK_DATE: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    timestamps: true,
    tableName: 'BALANCES_T'
  });

  db.models.TRANSACTIONS_T =  db.sequelize.define('TRANSACTIONS_T', {
    TRANSACTION_DATA_HASH: {
      type: DataTypes.STRING,
      allowNull: false
    },
    TRANSACTION_NONCE: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    TOKEN_ISSUER_USER_ID: {
      type: DataTypes.UUID,
      allowNull: false
    },
    TOKEN_ISSUER_ID_PSEUDONYM: {
      type: DataTypes.STRING,
      allowNull: false
    },
    TOKEN_ISSUER_ID_PUBLIC_KEY: {
      type: DataTypes.STRING,
      allowNull: false
    },
    FROM_USER_ID: {
      type: DataTypes.UUID,
      allowNull: false
    },
    FROM_USER_ID_PSEUDONYM: {
      type: DataTypes.STRING,
      allowNull: false
    },
    FROM_USER_ID_PUBLIC_KEY: {
      type: DataTypes.STRING,
      allowNull: false
    },
    TO_USER_ID: {
      type: DataTypes.UUID,
      allowNull: true
    },
    TO_USER_ID_PSEUDONYM: {
      type: DataTypes.STRING,
      allowNull: true
    },
    TO_USER_ID_PUBLIC_KEY: {
      type: DataTypes.STRING,
      allowNull: true
    },
    RAW_INTERNAL_TRANSACTION_DATA: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    INTERNAL_TRANSACTION_DATA: {
      type: DataTypes.JSON,
      allowNull: true
    },
    INTERNAL_TRANSACTION_DATA_HASH: {
      type: DataTypes.STRING,
      allowNull: true
    },
    TRANSACTION_CODE: {
      type: DataTypes.STRING,
      allowNull: false
    },
    TRANSACTION_VALUE: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    RAW_TRANSACTION_DATA: {      
      type: DataTypes.TEXT,
      allowNull: false
    },
    TRANSACTION_DATA: {      
      type: DataTypes.JSON,
      allowNull: false
    },
    SIGNED_TRANSACTION_HASH: {
      type: DataTypes.JSON,
      allowNull: false
    },
    TRANSACTION_DATE: {
      type: DataTypes.DATE,
      allowNull: true
    },
  }, {
    timestamps: true,
    tableName: 'TRANSACTIONS_T'
  });


  db.models.GROUPS_T = db.sequelize.define('GROUPS_T', {
    GROUP_ID: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    GROUP_OWNER: {
      type: DataTypes.UUID,
      allowNull: false
    },
    GROUP_NAME: {
      type: DataTypes.STRING,
      allowNull: true
    },
    CREATE_GROUP_TRANSACTION: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    timestamps: true,
    tableName: 'GROUPS_T'
  });


  db.models.GROUP_USERS_T = db.sequelize.define('GROUP_USERS_T', {
    GROUP_USER_ID: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    GROUP_ID: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false
    },
    USER_ID: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    PERMISSION_TRANSACTION: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    timestamps: true,
    tableName: 'GROUP_USERS_T'
  });
  // console.log(AUTH_T === db.sequelize.models.AUTH_T);
  // console.log(USER_T === db.sequelize.models.USER_T);
  // console.log(USER_PSEUDONYM_HIST_T === db.sequelize.models.USER_PSEUDONYM_HIST_T);
  // console.log(MEMES_T === db.sequelize.models.MEMES_T);
  // console.log(MEME_EDGES_T === db.sequelize.models.MEME_EDGES_T);
  // console.log(PUBLIC_KEY_ROTATION_T === db.sequelize.models.PUBLIC_KEY_ROTATION_T);
  // console.log(BALANCES_T === db.sequelize.models.BALANCES_T);
  // console.log(TRANSACTIONS_T === db.sequelize.models.TRANSACTIONS_T);

  // db.models.MEMES_T.belongsTo(
  //   db.models.TRANSACTIONS_T, 
  //   {
  //     foreignKey : "T_HASH_OF_MEME",// "TRANSACTION_DATA_HASH", //"T_HASH_OF_MEME",
  //     targetKey  : "TRANSACTION_DATA_HASH",
  //     as: 'user'
  //   }
  // )

  if ("reset" in options){
    await db.sequelize.sync({ force: true });
  }
  return db
}

