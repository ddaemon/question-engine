export async function verify_remove_transaction(req_body, request_transaction_data, user_uuid, db){
    if(Object.keys(request_transaction_data).length != 11){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Not correct number of keys"
        })
    }
    let keys_needed = [
        'token_issuer_id_pseudonym',
        'token_issuer_id_public_key', 
        'from_user_id_pseudonym',
        'signing_key',
        'to_user_id_pseudonym',
        'to_user_id_public_key',
        'value',
        'transaction_code',
        'transaction_date',
        'nonce',
        'previous_transaction_hash'
    ]
    if (  !keys_needed.every(key => Object.keys(request_transaction_data).includes(key))  ) {
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Does not have correct keys in TRANSACTION_DATA"
        })
    }


    // Check for issuer and user in the database
    let get_issuer_uuid = await db.models.USER_T.findOne({
        where: {
            PSEUDONYM   : request_transaction_data.token_issuer_id_pseudonym,
            PUBLIC_KEY  : request_transaction_data.token_issuer_id_public_key
        }
    })
    if (get_issuer_uuid == null){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Issuer account can't be found"
        })
    }
    let get_to_user_uuid = await db.models.USER_T.findOne({
        where: {
            PSEUDONYM   : request_transaction_data.to_user_id_pseudonym,
            PUBLIC_KEY  : request_transaction_data.to_user_id_public_key
        }
    })
    if (get_to_user_uuid == null){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "User you want to send funds too can't be found"
        })
    }

    // Check balance
    let check_token_balance = await db.models.BALANCES_T.findOne({
        where: {
            USER_TOKEN  : get_issuer_uuid.dataValues.USER_ID,
            TOKEN_OWNER : user_uuid

        }
    })
    if (check_token_balance == null){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Can't find balance, you sure you have this token?"
        })
    }

    if (parseInt(request_transaction_data.value) > parseInt(check_token_balance.dataValues.TOKEN_AMOUNT) ){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Brah you don't have that many tokens to send"
        })
    }
    
    // Add Transaciton
    let add_transaction = await db.models.TRANSACTIONS_T.build({
        TRANSACTION_DATA_HASH     : req_body.hash,
        TRANSACTION_NONCE         : request_transaction_data.nonce,
        TOKEN_ISSUER_USER_ID      : get_issuer_uuid.dataValues.USER_ID,
        TOKEN_ISSUER_ID_PSEUDONYM : request_transaction_data.token_issuer_id_pseudonym, 
        TOKEN_ISSUER_ID_PUBLIC_KEY: request_transaction_data.token_issuer_id_public_key,
        FROM_USER_ID              : user_uuid,
        FROM_USER_ID_PSEUDONYM    : request_transaction_data.from_user_id_pseudonym,
        FROM_USER_ID_PUBLIC_KEY   : request_transaction_data.signing_key,
        TO_USER_ID                : get_to_user_uuid.dataValues.USER_ID,
        TO_USER_ID_PSEUDONYM      : request_transaction_data.to_user_id_pseudonym,
        TO_USER_ID_PUBLIC_KEY     : request_transaction_data.to_user_id_public_key,
        TRANSACTION_CODE          : request_transaction_data.transaction_code,
        TRANSACTION_VALUE         : request_transaction_data.value,
        RAW_TRANSACTION_DATA      : req_body.data,
        TRANSACTION_DATA          : request_transaction_data,
        SIGNED_TRANSACTION_HASH   : req_body.signature_of_hash,
        TRANSACTION_DATE          : request_transaction_data.transaction_date
    })
    await add_transaction.save()

    // 
    await db.models.BALANCES_T.increment(
        {
            TOKEN_AMOUNT: - parseInt(request_transaction_data.value)
        },
        { where: 
            { 
                USER_TOKEN  : get_issuer_uuid.dataValues.USER_ID,
                TOKEN_OWNER : user_uuid
            } 
        }
    )
    // Check is token user is being sent is registered in BALANCES_T
    let check_token_to_balance = await db.models.BALANCES_T.findOne({
        where: {
            USER_TOKEN  : get_issuer_uuid.dataValues.USER_ID,
            TOKEN_OWNER : get_to_user_uuid.dataValues.USER_ID

        }
    })
    if (check_token_to_balance == null){
        return ({
            "response_type" : "ERROR",
            "response_code" : "INVALID_DATA_JSON",
            "description"   : "There is no balance to remove from",
        })
    } else {
        await db.models.BALANCES_T.increment(
            {
                TOKEN_AMOUNT: -parseInt(request_transaction_data.value)
            },
            { where: 
                { 
                    USER_TOKEN  : get_issuer_uuid.dataValues.USER_ID,
                    TOKEN_OWNER : get_to_user_uuid.dataValues.USER_ID
                } 
            }
        )
    }
    return ({
        "response_type" : "SUCCESS",
        "response_code" : "TRANSACTION_SUCCESSFUL",
        "description"   : "Ya that transaction you sent, it just went through",
    })
}