
import { ethers } from "ethers"

export async function verify_meme_transaction(request_body, request_transaction_data, user_uuid, db){
    if(Object.keys(request_transaction_data).length <= 10 ){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : `Not correct number of keys you had ${Object.keys(request_transaction_data).length} keys`
        })
    }
    let keys_needed = [
        'token_issuer_id_pseudonym',
        'token_issuer_id_public_key', 
        'from_user_id_pseudonym',
        'signing_key',
        'to_user_id_pseudonym',
        'to_user_id_public_key',
        'value',
        'transaction_code',
        'transaction_date',
        'nonce',
        'previous_transaction_hash',
        'data_hash'
    ]
    if (  !keys_needed.every(key => Object.keys(request_transaction_data).includes(key))  ) {
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : `Does not have correct keys in TRANSACTION_DATA`,
            "keys_needed"    : `${ (keys_needed.sort() )}`,
            "keys_sentaa"    : `${ ( Object.keys(request_transaction_data).sort() )}`
        })
    }
    


    if(request_transaction_data.to_user_id_pseudonym != "Zero" || request_transaction_data.to_user_id_public_key != '0x0000000000000000000000000000000000000000'){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "You must set to_user_id_pseudonym = Zero and to_user_id_public_key to 0x0000000000000000000000000000000000000000 to send a meme"
        })
    }


    // Query the Balances table to make sure transaction is valid
    // Find token UUID
    let find_token_uuid = await db.models.USER_T.findOne({
        where: {
            PSEUDONYM   : request_transaction_data.token_issuer_id_pseudonym,
            PUBLIC_KEY  : ethers.utils.getAddress(request_transaction_data.token_issuer_id_public_key)
        }
    })
    if (find_token_uuid == null){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Can't find your balance"
        })
    }
    let check_token_balance = await db.models.BALANCES_T.findOne({
        where: {
            USER_TOKEN  : find_token_uuid.dataValues.USER_ID,
            TOKEN_OWNER : user_uuid

        }
    })

    // Check if user has token balance and take it away
    // #TODO ADD TEST FOR THESE
    if (check_token_balance == null){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Token does not exist, TOKEN_ISSUER can not be found"
        })
    }
    // Make sure we can actually subtract balance without going negative
    if (parseInt(request_transaction_data.value) > parseInt(check_token_balance.dataValues.TOKEN_AMOUNT) ){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Brah you don't have that many tokens, memes aren't free you know"
        })
    }

    // Check if the MEME itself is valid
    let meme_data = null
    console.log(request_body.internal_data)
    try {
        meme_data = JSON.parse(request_body.internal_data)
    }
    catch {
        return({
            "response_type" : "ERROR",
            "response_code" : "INTERNAL_DATA_JSON_NOT_PARSABLE",
            "description"   : "field data can't be parsed by JSON.parse"
        })
    }
    let calculated_meme_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(request_body.internal_data))
    if (calculated_meme_hash != request_transaction_data.data_hash){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "internal_data_hash in signed transaction does not match hash of internal_data"
        })
    }
    let meme_keys_needed = [
        "created",
        "type",
        "meme_type_code",
        "permissions"

    ]
    if (  !meme_keys_needed.every(key => Object.keys(meme_data).includes(key))  ) {
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_INTERNAL_DATA_JSON",
            "description"    : "Transaction type MEME "
        })
    }
    if ( meme_data.type != "text" || !("text" in meme_data) ) {
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_INTERNAL_DATA_JSON",
            "description"    : "Only type text supported at this time"
        })
    }
    if ( !("title"  in meme_data.text) || !("body"  in meme_data.text) ) {
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_INTERNAL_DATA_JSON",
            "description"    : "text requires a body and title keys"
        })
    }
    // Check meme_type_code
    let meme_type_list = ["QUESTION", "ROOT_QUESTION", "STATEMENT", "ROOT_STATEMENT", "CONTEXT", "X"]
    if( !(meme_type_list.includes(meme_data.meme_type_code)) ){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_INTERNAL_DATA_JSON",
            "description"    : 'Invalid MEME type must be in the following list ["QUESTION", "ROOT_QUESTION", "STATEMENT", "ROOT_STATEMENT", "CONTEXT", "X"]'
        })
    }
    if ( meme_data.meme_type_code != "ROOT_QUESTION" || meme_data.meme_type_code != "ROOT_QUESTION" ){
        if( !("root_meme" in meme_data)){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_INTERNAL_DATA_JSON",
                "description"    : `You need to respond to a ROOT_MEME or ROOT_QUESTION with the type of meme you chose`
            })
        }
    }
    // Check permissions
    let permissions_list = [
        "PUBLIC", 
        "PRIVATE", 
        "ONLY_OP", 
        "ONLY_THIS_MEME", 
        "ONLY_THIS_THREAD",
        "REQUIRE_PAYMENT_RESPONDER_AND_ROOT_OWNER",
        "REQUIRE_PAYMENT_RESPONDER",
        "REQUIRE_PAYMENT_PERSONAL"]
    if(  !(permissions_list.includes(meme_data.permissions)) ){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_INTERNAL_DATA_JSON",
            "description"    : 'Invalid permission type must be in the following list ["PUBLIC", "PRIVATE", "ONLY_OP", "ONLY_THIS_MEME", "ONLY_THIS_THREAD"]'
        })
    }


    // Insert MEME into database
    let response_to_meme = null
    if ( "response_to_meme"  in meme_data){
        response_to_meme = meme_data.response_to_meme
    }


    // Take away users token
    let new_balance = parseInt(check_token_balance.dataValues.TOKEN_AMOUNT) - parseInt(request_transaction_data.value)
    await db.models.BALANCES_T.increment(
        {
            TOKEN_AMOUNT: -parseInt(request_transaction_data.value)
        },
        { where: 
            { 
                USER_TOKEN  : find_token_uuid.dataValues.USER_ID,
                TOKEN_OWNER : user_uuid
            } 
        }
    )
    let add_meme = await db.models.MEMES_T.build({
        T_HASH_OF_MEME            : request_body.hash,
        AUTHOR_USER_ID            : user_uuid,
        RAW_MEME_CONTENT          : request_body.internal_data,
        HASH_OF_MEME              : request_transaction_data.data_hash,
        MEME_CONTENT              : meme_data,
        MEME_TYPE_CODE            : meme_data.meme_type_code,
        MEME_PERMISSIONS          : meme_data.permissions,
        RESPONSE_TO_MEME          : response_to_meme

    })
    await add_meme.save()

    // Insert the transaction itself
    let add_transaction = await db.models.TRANSACTIONS_T.build({
        TRANSACTION_DATA_HASH     : request_body.hash,
        TRANSACTION_NONCE         : request_transaction_data.nonce,
        TOKEN_ISSUER_USER_ID      : user_uuid,
        TOKEN_ISSUER_ID_PSEUDONYM : request_transaction_data.token_issuer_id_pseudonym, 
        TOKEN_ISSUER_ID_PUBLIC_KEY: ethers.utils.getAddress(request_transaction_data.token_issuer_id_public_key),
        FROM_USER_ID              : user_uuid,
        FROM_USER_ID_PSEUDONYM    : request_transaction_data.from_user_id_pseudonym,
        FROM_USER_ID_PUBLIC_KEY   : ethers.utils.getAddress(request_transaction_data.signing_key),
        TO_USER_ID                : user_uuid,
        TO_USER_ID_PSEUDONYM      : request_transaction_data.to_user_id_pseudonym,
        TO_USER_ID_PUBLIC_KEY     : ethers.utils.getAddress(request_transaction_data.to_user_id_public_key),
        TRANSACTION_CODE          : request_transaction_data.transaction_code,
        TRANSACTION_VALUE         : request_transaction_data.value,
        RAW_TRANSACTION_DATA      : request_body.data,
        TRANSACTION_DATA          : request_transaction_data,
        SIGNED_TRANSACTION_HASH   : request_body.signature_of_hash,
        TRANSACTION_DATE          : request_transaction_data.transaction_date
    })
    await add_transaction.save()

    // #TODO better response code
    return ({
        "response_type" : "SUCCESS",
        "response_code" : "TRANSACTION_SUCCESSFUL",
        "description"   : `Meme send and your new balance of that token is ${new_balance}`
    })
}