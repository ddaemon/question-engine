export async function verify_mint_transaction(req_body, request_transaction_data, user_uuid, db){
    console.log("\n\n nnn \n\n")
    console.log(req_body)
    if(Object.keys(request_transaction_data).length != 10 && Object.keys(request_transaction_data).length != 11){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Not correct number of keys"
        })
    }
    let keys_needed = [
        'token_issuer_id_pseudonym',
        'token_issuer_id_public_key', 
        'from_user_id_pseudonym',
        'signing_key',
        'to_user_id_pseudonym',
        'to_user_id_public_key',
        'value',
        'transaction_code',
        'transaction_date',
        'nonce'
    ]
    if (  !keys_needed.every(key => Object.keys(request_transaction_data).includes(key))  ) {
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Does not have correct keys in TRANSACTION_DATA"
        })
    }

    // Verify fields match
    if(request_transaction_data.token_issuer_id_pseudonym != request_transaction_data.from_user_id_pseudonym){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Invalid data within transaction"
        })
    }
    if(request_transaction_data.from_user_id_pseudonym != request_transaction_data.to_user_id_pseudonym){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Invalid data within transaction"
        })
    }
    if(request_transaction_data.token_issuer_id_public_key != request_transaction_data.signing_key){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Invalid data within transaction"
        })
    }
    if(request_transaction_data.signing_key != request_transaction_data.to_user_id_public_key){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Invalid data within transaction"
        })
    }
    // Insert the transaction itself
    let add_transaction = await db.models.TRANSACTIONS_T.build({
        TRANSACTION_DATA_HASH     : req_body.hash,
        TRANSACTION_NONCE         : request_transaction_data.nonce,
        TOKEN_ISSUER_USER_ID      : user_uuid,
        TOKEN_ISSUER_ID_PSEUDONYM : request_transaction_data.token_issuer_id_pseudonym, 
        TOKEN_ISSUER_ID_PUBLIC_KEY: request_transaction_data.token_issuer_id_public_key,
        FROM_USER_ID              : user_uuid,
        FROM_USER_ID_PSEUDONYM    : request_transaction_data.from_user_id_pseudonym,
        FROM_USER_ID_PUBLIC_KEY   : request_transaction_data.signing_key,
        TO_USER_ID                : user_uuid,
        TO_USER_ID_PSEUDONYM      : request_transaction_data.to_user_id_pseudonym,
        TO_USER_ID_PUBLIC_KEY     : request_transaction_data.to_user_id_public_key,
        TRANSACTION_CODE          : request_transaction_data.transaction_code,
        TRANSACTION_VALUE         : request_transaction_data.value,
        RAW_TRANSACTION_DATA      : req_body.data,
        TRANSACTION_DATA          : request_transaction_data,
        SIGNED_TRANSACTION_HASH   : req_body.signature_of_hash,
        TRANSACTION_DATE          : request_transaction_data.transaction_date
    })
    await add_transaction.save()
    
    let check_token_balance = await db.models.BALANCES_T.findAll({
        where: {
            USER_TOKEN  : user_uuid,
            TOKEN_OWNER : user_uuid

        }
    })
    if (check_token_balance.length == 0){
        let balances_t_insert = await db.models.BALANCES_T.build({
            USER_TOKEN  : user_uuid,
            TOKEN_OWNER : user_uuid,
            TOKEN_AMOUNT: request_transaction_data.value
        })
        await balances_t_insert.save()
        let tokens_t_insert = await db.models.TOKENS_T.build({
            USER_TOKEN      : user_uuid,
            FAUCET_VALUE    : 4,
            FAUCET_FLOW     : 1200000, // 20 min in milliseconds 1000*60*20
            FAUCET_STATUS   : "ON",
            TOKEN_AMOUNT    : request_transaction_data.value,
            TOKEN_STATUS    : "LIQUID",
            FAUCET_LAST_USE : new Date(  (new Date(request_transaction_data.transaction_date)).getTime() - 1200000  ) 
        })
        await tokens_t_insert.save()
        return ({
            "response_type" : "SUCCESS",
            "response_code" : "TRANSACTION_SUCCESSFUL",
            "description"   : "Ya that transaction you sent, it just went through",
        })
    }
    else {
        let new_balance = await db.models.BALANCES_T.increment(
            {
                TOKEN_AMOUNT: parseInt(request_transaction_data.value)
            },
            { where: 
                { 
                    USER_TOKEN  : user_uuid,
                    TOKEN_OWNER : user_uuid
                } 
            }
        )
        return ({
            "response_type" : "SUCCESS",
            "response_code" : "TRANSACTION_SUCCESSFUL",
        })
    }
}