export async function verify_use_faucet_transaction(req_body, request_transaction_data, user_uuid, db){
    if(Object.keys(request_transaction_data).length != 10){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Not correct number of keys"
        })
    }
    let keys_needed = [
        'token_issuer_id_pseudonym',
        'token_issuer_id_public_key', 
        'from_user_id_pseudonym',
        'signing_key',
        'to_user_id_pseudonym',
        'to_user_id_public_key',
        'transaction_code',
        'transaction_date',
        'nonce',
        'previous_transaction_hash'
    ]
    if (  !keys_needed.every(key => Object.keys(request_transaction_data).includes(key))  ) {
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Does not have correct keys in TRANSACTION_DATA"
        })
    }

    // Check if someone is sending a token to themselves, cause that is how faucet's work
    if(request_transaction_data.from_user_id_pseudonym != request_transaction_data.to_user_id_pseudonym){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Invalid data within transaction pseudonyms"
        })
    }
    if(request_transaction_data.signing_key != request_transaction_data.to_user_id_public_key){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Invalid data within transaction keys"
        })
    }

    // Check for issuer and user in the database
    let get_issuer_uuid = await db.models.USER_T.findOne({
        where: {
            PSEUDONYM   : request_transaction_data.token_issuer_id_pseudonym,
            PUBLIC_KEY  : request_transaction_data.token_issuer_id_public_key
        }
    })
    if (get_issuer_uuid == null){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Token Issuer account can not be found"
        })
    }
    // Check for TOKEN in Database
    let get_token_data = await db.models.TOKENS_T.findOne({
        where: {
            USER_TOKEN   : get_issuer_uuid.dataValues.USER_ID
        }
    })
    if (get_token_data == null){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "That token does not seem to exist"
        })
    }

    let faucet_open_milliseconds = (new Date(get_token_data.dataValues.FAUCET_LAST_USE)).getTime() + get_token_data.dataValues.FAUCET_FLOW 
    if ( faucet_open_milliseconds > (new Date(request_transaction_data.transaction_date)).getTime() ){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : `Faucet is not open please wait till ${new Date(faucet_open_milliseconds)}`
        })
    }

    // Check balance
    let check_token_balance = await db.models.BALANCES_T.findOne({
        where: {
            USER_TOKEN  :  get_issuer_uuid.dataValues.USER_ID,
            TOKEN_OWNER :  user_uuid

        }
    })
    // Update Balances
    if (check_token_balance == null){
        let balances_t_insert = await db.models.BALANCES_T.build({
            USER_TOKEN  : user_uuid,
            TOKEN_OWNER : user_uuid,
            TOKEN_AMOUNT: 4 // #TODO
        })
        await balances_t_insert.save()
    }

    
    // Add Transaction
    let add_transaction = await db.models.TRANSACTIONS_T.build({
        TRANSACTION_DATA_HASH     : req_body.hash,
        TRANSACTION_NONCE         : request_transaction_data.nonce,
        TOKEN_ISSUER_USER_ID      : get_issuer_uuid.dataValues.USER_ID,
        TOKEN_ISSUER_ID_PSEUDONYM : request_transaction_data.token_issuer_id_pseudonym, 
        TOKEN_ISSUER_ID_PUBLIC_KEY: request_transaction_data.token_issuer_id_public_key,
        FROM_USER_ID              : user_uuid,
        FROM_USER_ID_PSEUDONYM    : request_transaction_data.from_user_id_pseudonym,
        FROM_USER_ID_PUBLIC_KEY   : request_transaction_data.signing_key,
        TO_USER_ID                : user_uuid,
        TO_USER_ID_PSEUDONYM      : request_transaction_data.to_user_id_pseudonym,
        TO_USER_ID_PUBLIC_KEY     : request_transaction_data.to_user_id_public_key,
        TRANSACTION_CODE          : request_transaction_data.transaction_code,
        TRANSACTION_VALUE         : get_token_data.dataValues.FAUCET_VALUE,
        RAW_TRANSACTION_DATA      : req_body.data,
        TRANSACTION_DATA          : request_transaction_data,
        SIGNED_TRANSACTION_HASH   : req_body.signature_of_hash,
        TRANSACTION_DATE          : request_transaction_data.transaction_date
    })
    await add_transaction.save()

    // Check is token user is being sent is registered in BALANCES_T
    let check_token_to_balance = await db.models.BALANCES_T.findOne({
        where: {
            USER_TOKEN  : get_issuer_uuid.dataValues.USER_ID,
            TOKEN_OWNER : user_uuid

        }
    })
    if (check_token_to_balance == null){
        let balances_transaction = await db.models.BALANCES_T.build({
            USER_TOKEN   :  get_issuer_uuid.dataValues.USER_ID,
            TOKEN_OWNER  :  user_uuid,
            TOKEN_AMOUNT :  parseInt(get_token_data.dataValues.FAUCET_VALUE)
        })
        await balances_transaction.save()
        return ({
            "response_type" : "SUCCESS",
            "response_code" : "TRANSACTION_SUCCESSFUL",
            "description"   : "Ya that transaction you sent, it just went through",
            "Balance"       : 4
        })
    } else {
        await db.models.BALANCES_T.increment(
            {
                TOKEN_AMOUNT: parseInt(get_token_data.dataValues.FAUCET_VALUE)
            },
            { where: 
                { 
                    USER_TOKEN  : get_issuer_uuid.dataValues.USER_ID,
                    TOKEN_OWNER : user_uuid
                } 
            }
        )
        return ({
            "response_type" : "SUCCESS",
            "response_code" : "TRANSACTION_SUCCESSFUL",
            "description"   : "Ya that transaction you sent, it just went through",
            "Balance"       : check_token_to_balance.dataValues.TOKEN_AMOUNT + parseInt(get_token_data.dataValues.FAUCET_VALUE)
        })
    }
}