import { ethers } from "ethers"
export async function verify_burn_transaction(req_body, request_transaction_data, user_uuid, db){
    if(Object.keys(request_transaction_data).length != 11){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Not correct number of keys"
        })
    }
    let keys_needed = [
        'token_issuer_id_pseudonym',
        'token_issuer_id_public_key', 
        'from_user_id_pseudonym',
        'signing_key',
        'to_user_id_pseudonym',
        'to_user_id_public_key',
        'value',
        'transaction_code',
        'transaction_date',
        'nonce',
        'previous_transaction_hash'
    ]
    if (  !keys_needed.every(key => Object.keys(request_transaction_data).includes(key))  ) {
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Does not have correct keys in TRANSACTION_DATA"
        })
    }

    if(request_transaction_data.to_user_id_pseudonym != "Zero" || request_transaction_data.to_user_id_public_key != '0x0000000000000000000000000000000000000000'){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "You must set to_user_id_pseudonym = Zero and to_user_id_public_key to 0x0000000000000000000000000000000000000000 to send a meme"
        })
    }

    // Query the Balances table to make sure transaction is valid
    // Find token UUID
    let find_token_uuid = await db.models.USER_T.findOne({
        where: {
            PUBLIC_KEY  : ethers.utils.getAddress(request_transaction_data.token_issuer_id_public_key),
            PSEUDONYM   : request_transaction_data.token_issuer_id_pseudonym 
        }
    })
    if (find_token_uuid == null){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Can't find your balance"
        })
    }
    let check_token_balance = await db.models.BALANCES_T.findOne({
        where: {
            USER_TOKEN  : find_token_uuid.dataValues.USER_ID,
            TOKEN_OWNER : user_uuid

        }
    })
    // #TODO ADD TEST FOR THESE
    if (check_token_balance == null){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Token does not exist, TOKEN_ISSUER can not be found"
        })
    }
    // Make sure we can actually subtract balance without going negative
    if (parseInt(request_transaction_data.value) > parseInt(check_token_balance.dataValues.TOKEN_AMOUNT) ){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Brah you don't have that many tokens to burn"
        })
    }
    let new_balance = parseInt(check_token_balance.dataValues.TOKEN_AMOUNT) - parseInt(request_transaction_data.value)
    await db.models.BALANCES_T.increment(
        {
            TOKEN_AMOUNT: - parseInt(request_transaction_data.value)
        },
        { where: 
            { 
                USER_TOKEN  : find_token_uuid.dataValues.USER_ID,
                TOKEN_OWNER : user_uuid
            } 
        }
    )

    // Insert the transaction itself
    let add_transaction = await db.models.TRANSACTIONS_T.build({
        TRANSACTION_DATA_HASH     : req_body.hash,
        TRANSACTION_NONCE         : request_transaction_data.nonce,
        TOKEN_ISSUER_USER_ID      : user_uuid,
        TOKEN_ISSUER_ID_PSEUDONYM : (request_transaction_data.token_issuer_id_pseudonym), 
        TOKEN_ISSUER_ID_PUBLIC_KEY: ethers.utils.getAddress(request_transaction_data.token_issuer_id_public_key),
        FROM_USER_ID              : user_uuid,
        FROM_USER_ID_PSEUDONYM    : request_transaction_data.from_user_id_pseudonym,
        FROM_USER_ID_PUBLIC_KEY   : ethers.utils.getAddress(request_transaction_data.signing_key),
        TO_USER_ID                : user_uuid,
        TO_USER_ID_PSEUDONYM      : request_transaction_data.to_user_id_pseudonym,
        TO_USER_ID_PUBLIC_KEY     : ethers.utils.getAddress(request_transaction_data.to_user_id_public_key),
        TRANSACTION_CODE          : request_transaction_data.transaction_code,
        TRANSACTION_VALUE         : request_transaction_data.value,
        RAW_TRANSACTION_DATA      : req_body.data,
        TRANSACTION_DATA          : request_transaction_data,
        SIGNED_TRANSACTION_HASH   : req_body.signature_of_hash,
        TRANSACTION_DATE          : request_transaction_data.transaction_date
    })
    await add_transaction.save()
    
    return ({
        "response_type" : "SUCCESS",
        "response_code" : "TRANSACTION_SUCCESSFUL",
        "description"   : `You got yourself a new balance ${new_balance}`
    })
}