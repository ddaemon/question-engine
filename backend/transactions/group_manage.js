
import { ethers } from "ethers"

export async function verify_group_manage_transaction(request_body, request_transaction_data, user_uuid, db){
    if(Object.keys(request_transaction_data).length <= 10 ){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : `Not correct number of keys you had ${Object.keys(request_transaction_data).length} keys`
        })
    }
    let keys_needed = [
        'token_issuer_id_pseudonym',
        'token_issuer_id_public_key', 
        'from_user_id_pseudonym',
        'signing_key',
        'to_user_id_pseudonym',
        'to_user_id_public_key',
        'value',
        'transaction_code',
        'transaction_date',
        'nonce',
        'previous_transaction_hash',
        'data_hash'
    ]
    if (  !keys_needed.every(key => Object.keys(request_transaction_data).includes(key))  ) {
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : `Does not have correct keys in TRANSACTION_DATA`,
            "keys_needed"    : `${ (keys_needed.sort() )}`,
            "keys_sentaa"    : `${ ( Object.keys(request_transaction_data).sort() )}`
        })
    }
    


    let get_to_user_uuid = await db.models.USER_T.findOne({
        where: {
            PSEUDONYM   : request_transaction_data.to_user_id_pseudonym,
            PUBLIC_KEY  : request_transaction_data.to_user_id_public_key
        }
    })
    if (get_to_user_uuid == null){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "User you want to send funds too can't be found"
        })
    }


    // Query the Balances table to make sure transaction is valid
    // Find token UUID
    let find_token_uuid = await db.models.USER_T.findOne({
        where: {
            PSEUDONYM   : request_transaction_data.token_issuer_id_pseudonym,
            PUBLIC_KEY  : ethers.utils.getAddress(request_transaction_data.token_issuer_id_public_key)
        }
    })
    if (find_token_uuid == null){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Can't find your balance"
        })
    }
    let check_token_balance = await db.models.BALANCES_T.findOne({
        where: {
            USER_TOKEN  : find_token_uuid.dataValues.USER_ID,
            TOKEN_OWNER : user_uuid

        }
    })

    // Check if user has token balance and take it away
    // #TODO ADD TEST FOR THESE
    if (check_token_balance == null){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Token does not exist, TOKEN_ISSUER can not be found"
        })
    }
    // Make sure we can actually subtract balance without going negative
    if (parseInt(request_transaction_data.value) > parseInt(check_token_balance.dataValues.TOKEN_AMOUNT) ){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Brah you don't have that many tokens, memes aren't free you know"
        })
    }

    // Check if the GROUP itself is valid
    let group_data = null
    console.log(request_body.internal_data)
    try {
      group_data = JSON.parse(request_body.internal_data)
    }
    catch {
        return({
            "response_type" : "ERROR",
            "response_code" : "INTERNAL_DATA_JSON_NOT_PARSABLE",
            "description"   : "field data can't be parsed by JSON.parse"
        })
    }
    let calculated_group_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(request_body.internal_data))
    if (calculated_group_hash != request_transaction_data.data_hash){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "internal_data_hash in signed transaction does not match hash of internal_data"
        })
    }
    let group_keys_needed = [
        "name",
        "add_or_remove"
    ]
    if (  !group_keys_needed.every(key => Object.keys(group_data).includes(key))  ) {
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_INTERNAL_DATA_JSON",
            "description"    : "Transaction type GROUP "
        })
    }


    // Check for issuer and user in the database
    let get_issuer_uuid = await db.models.USER_T.findOne({
        where: {
            PSEUDONYM   : request_transaction_data.token_issuer_id_pseudonym,
            PUBLIC_KEY  : request_transaction_data.token_issuer_id_public_key
        }
    })
    if (get_issuer_uuid == null){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Issuer account can't be found"
        })
    }
    console.log("NAME")
    console.log(group_data.name)
    let check_group = await db.models.GROUPS_T.findOne({
      where: {
        GROUP_NAME                : group_data.name,
        GROUP_OWNER               : get_issuer_uuid.dataValues.USER_ID
      }
    })
    console.log("check_group")
    console.log(check_group)
    if (check_group == null){
      return ({
        "response_type"  : "ERROR",
        "response_code"  : "INVALID_GROUP",
        "description"    : "GROUP does not exist to manage "
      })
    }
    // Check if user is in group
    let check_group_users = await db.models.GROUPS_T.findOne({
      where: {
        GROUP_ID                 : check_group.dataValues.GROUP_ID,
        GROUP_OWNER              : get_to_user_uuid.dataValues.USER_ID
      }
    })
    if (check_group_users == null){
      if(group_data.add_or_remove == 'remove'){
        return ({
          "response_type"  : "ERROR",
          "response_code"  : "NOT_IN_GROUP",
          "description"    : "GROUP does not exist to manage "
        })
      }
    }
    // Take away users token
    await db.models.BALANCES_T.increment(
      {
          TOKEN_AMOUNT: - parseInt(request_transaction_data.value)
      },
      { where: 
          { 
              USER_TOKEN  : get_issuer_uuid.dataValues.USER_ID,
              TOKEN_OWNER : user_uuid
          } 
      }
  )
  // Check is token user is being sent is registered in BALANCES_T
  let check_token_to_balance = await db.models.BALANCES_T.findOne({
      where: {
          USER_TOKEN  : get_issuer_uuid.dataValues.USER_ID,
          TOKEN_OWNER : get_to_user_uuid.dataValues.USER_ID

      }
  })
  if (check_token_to_balance == null){
      let add_transaction = await db.models.BALANCES_T.build({
          USER_TOKEN   :  get_issuer_uuid.dataValues.USER_ID,
          TOKEN_OWNER  :  get_to_user_uuid.dataValues.USER_ID,
          TOKEN_AMOUNT :  parseInt(request_transaction_data.value)
      })
      await add_transaction.save()
  } else {
      await db.models.BALANCES_T.increment(
          {
              TOKEN_AMOUNT: parseInt(request_transaction_data.value)
          },
          { where: 
              { 
                  USER_TOKEN  : get_issuer_uuid.dataValues.USER_ID,
                  TOKEN_OWNER : get_to_user_uuid.dataValues.USER_ID
              } 
          }
      )
  }

    // Insert the transaction itself
    let add_transaction = await db.models.TRANSACTIONS_T.build({
        TRANSACTION_DATA_HASH     : request_body.hash,
        TRANSACTION_NONCE         : request_transaction_data.nonce,
        TOKEN_ISSUER_USER_ID      : user_uuid,
        TOKEN_ISSUER_ID_PSEUDONYM : request_transaction_data.token_issuer_id_pseudonym, 
        TOKEN_ISSUER_ID_PUBLIC_KEY: ethers.utils.getAddress(request_transaction_data.token_issuer_id_public_key),
        FROM_USER_ID              : user_uuid,
        FROM_USER_ID_PSEUDONYM    : request_transaction_data.from_user_id_pseudonym,
        FROM_USER_ID_PUBLIC_KEY   : ethers.utils.getAddress(request_transaction_data.signing_key),
        TO_USER_ID                : user_uuid,
        TO_USER_ID_PSEUDONYM      : request_transaction_data.to_user_id_pseudonym,
        TO_USER_ID_PUBLIC_KEY     : ethers.utils.getAddress(request_transaction_data.to_user_id_public_key),
        TRANSACTION_CODE          : request_transaction_data.transaction_code,
        TRANSACTION_VALUE         : request_transaction_data.value,
        RAW_TRANSACTION_DATA      : request_body.data,
        TRANSACTION_DATA          : request_transaction_data,
        SIGNED_TRANSACTION_HASH   : request_body.signature_of_hash,
        TRANSACTION_DATE          : request_transaction_data.transaction_date
    })
    await add_transaction.save()
    if(group_data.add_or_remove == 'remove'){
      let remove_user_from_group = await db.models.GROUP_USERS_T.destroy({
        where: {
          GROUP_ID               :  check_group.dataValues.GROUP_ID,
          USER_ID                :  get_to_user_uuid.dataValues.USER_ID,
        }
      })
      await remove_user_from_group.save()
    }
    if(group_data.add_or_remove == 'add'){
        let add_group_user = await db.models.GROUP_USERS_T.build({
          GROUP_ID               :  check_group.dataValues.GROUP_ID,
          USER_ID                :  get_to_user_uuid.dataValues.USER_ID,
          PERMISSION_TRANSACTION :  null
        })
        await add_group_user.save()
    }
    // #TODO better response code
    return ({
        "response_type" : "SUCCESS",
        "response_code" : "TRANSACTION_SUCCESSFUL",
        "description"   : `Group ${group_data.name} Eddited`
    })
}
