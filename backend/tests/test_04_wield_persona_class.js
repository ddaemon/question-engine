import { ethers } from "ethers";
import fetch from 'node-fetch';
import assert from 'assert'
import { persona_names } from "./persona_names.js"
import Persona from "../helper_functions/Persona.js"

let special_phase = "curve foster stay broccoli equal icon bamboo champion casino impact will damp"
let wallets = []
for (let i = 0; i < 10; i++) {
  wallets.push(ethers.Wallet.fromMnemonic(special_phase, `m/44'/60'/0'/0/${i}`))
}

let testing_url = "http://localhost:4000"


describe('Testing MEME Transactions', async function () {
    let Persona7 = null
    let Persona4 = null
    describe('Wield Persona with wallet[3] using Persona class', async function () {
        it('/api/query_memes Fetch a pseydonym for a Persona', async function () {
            let Persona3 = new Persona("EGREGORE", wallets[3].address, "", testing_url)
            Persona3.setPseudonym(  await Persona3.lookupPseudonym(Persona3.evm_address) )
            Persona3.setValidity(   await Persona3.check_persona_exists(Persona3.evm_address, Persona3.pseudonym) )
            assert.equal(Persona3.valid_persona, true)
        })
        it('Sign in after fetching Persona pseudonym', async function () {
            let Persona3 = new Persona("EGREGORE", wallets[3].address, "", testing_url)
            Persona3.setPseudonym(  await Persona3.lookupPseudonym(Persona3.evm_address) )
            Persona3.setValidity(   await Persona3.check_persona_exists(Persona3.evm_address, Persona3.pseudonym) )
            let sign_in_json =  await Persona3.generate_sign_in_hash()
            sign_in_json.signature_of_hash = await wallets[3].signMessage( sign_in_json.hash )
            let sign_in_state = Persona3.setSignedIn( await Persona3.sign_in(  sign_in_json )  )
            assert.equal(sign_in_state, true)
        })
        it('Sign in and fetch previous transaction metadata, store in Persona object', async function () {
            let Persona3 = new Persona("EGREGORE", wallets[3].address, "", testing_url)
            Persona3.setPseudonym(  await Persona3.lookupPseudonym(Persona3.evm_address) )
            Persona3.setValidity(   await Persona3.check_persona_exists(Persona3.evm_address, Persona3.pseudonym) )
            let sign_in_json =  await Persona3.generate_sign_in_hash()
            sign_in_json.signature_of_hash = await wallets[3].signMessage( sign_in_json.hash )
            let sign_in_result = await Persona3.sign_in(  sign_in_json )
            Persona3.setSignedIn(sign_in_result)
            Persona3.setPreviousTransaction(  await Persona3.get_previous_transaction() )
            assert.equal(Persona3.access_token != null, true)
        })
    })
    describe('Mint, TRANSFER, REMOVE, AND BURN using new and old Persona', async function () {
        it('Create Persona7 and MINT 24 tokens #TODO Balance', async function () {
            Persona7 = new Persona("EGREGORE", wallets[7].address, "", testing_url)
            let check_persona_exists = await Persona7.lookupPseudonym(Persona7.evm_address)
            assert.equal(check_persona_exists, false)
            Persona7.setPseudonym( persona_names[7] )
            let sign_in_json =  await Persona7.generate_sign_in_hash()
            sign_in_json.signature_of_hash = await wallets[7].signMessage( sign_in_json.hash )
            let sign_in_result = await Persona7.sign_in(  sign_in_json )
            Persona7.setSignedIn(sign_in_result)
            assert.equal(Persona7.access_token != null, true)
            let transaction_data = await Persona7.GENERATE_INITIAL_MINT_TRANSACTION(24)
            transaction_data.signature_of_hash = await wallets[7].signMessage( transaction_data.hash )
            let transaction_result = await Persona7.SEND_TRANSACTION(transaction_data)
            assert.equal("response_type" in transaction_result, true)
            assert.equal(transaction_result.response_type == "SUCCESS", true)
        })
        it('Wield Persona0 and Mint 24 Tokens #TODO Balance', async function () {
            Persona4 = new Persona("EGREGORE", wallets[4].address, "", testing_url)
            Persona4.setPseudonym( await Persona4.lookupPseudonym(Persona4.evm_address) )
            Persona4.setValidity(  await Persona4.check_persona_exists(Persona4.evm_address, Persona4.pseudonym) )
            let sign_in_json =  await Persona4.generate_sign_in_hash()
            sign_in_json.signature_of_hash = await wallets[4].signMessage( sign_in_json.hash )
            let sign_in_result = await Persona4.sign_in(  sign_in_json )
            Persona4.setSignedIn(sign_in_result)
            Persona4.setPreviousTransaction(  await Persona4.get_previous_transaction() )
            assert.equal(Persona4.access_token != null, true)
            let transaction_data = await Persona4.GENERATE_MINT_TRANSACTION(24)
            transaction_data.signature_of_hash = await wallets[4].signMessage( transaction_data.hash )
            let transaction_result = await Persona4.SEND_TRANSACTION(transaction_data)            
            assert.equal("response_type" in transaction_result, true)
            assert.equal(transaction_result.response_type == "SUCCESS", true)
        })
        it('TRANSFER 6 Persona7 tokens to Persona4 #TODO Balance', async function () {
            Persona7.setPreviousTransaction(  await Persona7.get_previous_transaction() )
            assert.equal(Persona7.access_token != null, true)
            let transaction_data = await Persona7.GENERATE_TRANSFER_TRANSACTION(
                Persona7.pseudonym,   // Token Issuer
                Persona7.evm_address,
                Persona4.pseudonym,   // To
                Persona4.evm_address,
                6
            )
            transaction_data.signature_of_hash = await wallets[7].signMessage( transaction_data.hash )
            let transaction_result = await Persona7.SEND_TRANSACTION(transaction_data)
            assert.equal("response_type" in transaction_result, true)
            assert.equal(transaction_result.response_type == "SUCCESS", true)
            // Persona4 should have 6 Persona7 Tokens 
        })
        it('TRANSFER 3 Persona7 tokens from Persona4 to Persona7 #TODO Balance', async function () {
            Persona4.setPreviousTransaction(  await Persona4.get_previous_transaction() )
            assert.equal(Persona4.access_token != null, true)
            let transaction_data = await Persona4.GENERATE_TRANSFER_TRANSACTION(
                Persona7.pseudonym,   // Token Issuer
                Persona7.evm_address,
                Persona7.pseudonym,   // To
                Persona7.evm_address,
                3
            )
            transaction_data.signature_of_hash = await wallets[4].signMessage( transaction_data.hash )
            let transaction_result = await Persona7.SEND_TRANSACTION(transaction_data)
            assert.equal("response_type" in transaction_result, true)
            assert.equal(transaction_result.response_type == "SUCCESS", true)
            let token_balance = await Persona4.get_my_token_balance(Persona7.pseudonym, Persona7.evm_address)
            assert.equal(token_balance.data.TOKEN_AMOUNT == 3, true)
        })
        it('BURN 1 TOKEN of issuer Persona7 by Persona4', async function () {
            Persona4.setPreviousTransaction(  await Persona4.get_previous_transaction() )
            assert.equal(Persona4.access_token != null, true)
            let transaction_data = await Persona4.GENERATE_BURN_TRANSACTION(
                Persona7.pseudonym,   // Token Issuer
                Persona7.evm_address,
                1
            )
            transaction_data.signature_of_hash = await wallets[4].signMessage( transaction_data.hash )
            let transaction_result = await Persona7.SEND_TRANSACTION(transaction_data)
            assert.equal("response_type" in transaction_result, true)
            assert.equal(transaction_result.response_type == "SUCCESS", true)
            let token_balance = await Persona4.get_my_token_balance(Persona7.pseudonym, Persona7.evm_address)
            assert.equal(token_balance.data.TOKEN_AMOUNT == 2, true)
        })
        it('REMOVE 1 token of issuer Persona7 from Persona4', async function () {
            Persona7.setPreviousTransaction(  await Persona7.get_previous_transaction() )
            assert.equal(Persona7.access_token != null, true)
            let transaction_data = await Persona7.GENERATE_REMOVE_TRANSACTION(
                Persona4.pseudonym,
                Persona4.evm_address,
                1
            )
            transaction_data.signature_of_hash = await wallets[7].signMessage( transaction_data.hash )
            let transaction_result = await Persona7.SEND_TRANSACTION(transaction_data)
            assert.equal("response_type" in transaction_result, true)
            assert.equal(transaction_result.response_type == "SUCCESS", true)
            let token_balance = await Persona4.get_my_token_balance(Persona7.pseudonym, Persona7.evm_address)
            assert.equal(token_balance.data.TOKEN_AMOUNT == 1, true)
        })
    })
})