import { ethers } from "ethers";
import fetch from 'node-fetch';
import assert from 'assert'

let special_phase = "curve foster stay broccoli equal icon bamboo champion casino impact will damp"
let wallets = []
for (let i = 0; i < 10; i++) {
  wallets.push(ethers.Wallet.fromMnemonic(special_phase, `m/44'/60'/0'/0/${i}`))
}

let testing_url = "http://localhost:3000"


describe('Testing MEME Transactions', async function () {
    let test_wallet_pseudonym_0 = "Name Zero" // Use wallet X
    let test_wallet_0_cookie = null
    let test_wallet_0_pervious_transaction_hash = null
    let test_wallet_pseudonym_1 = "Name One" // Use wallet Y
    let test_wallet_1_cookie = null
    let test_wallet_1_pervious_transaction_hash = null
    describe('/wield_persona', async function () {
        it('get_cookie test_wallet_0_cookie', async function () {
            const response = await fetch(
                testing_url + "/get_cookie",
                {
                    credentials: 'include'
                }
            );    
            test_wallet_0_cookie = response.headers.get('set-cookie')
            assert.equal(test_wallet_0_cookie.includes("token"), true)
        });
        it('make request with correct hash AND signature', async function () {
            let data = {
                "pseudonym" : test_wallet_pseudonym_0,
                "unix_time_ms" : (new Date()).getTime(),
                "signing_key" : wallets[X].address
            }
            let data_string = JSON.stringify(data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            let signed_hash = await wallets[X].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/wield_persona", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash
                    }),
                    headers: {
                        Cookie: test_wallet_0_cookie,
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code, "NOW_LOGGED_IN")
        });
        it('get_cookie wallet_4_cookie', async function () {
            const response = await fetch(
                testing_url + "/get_cookie",
                {
                    credentials: 'include'
                }
            );    
            test_wallet_1_cookie = response.headers.get('set-cookie')
            assert.equal(test_wallet_1_cookie.includes("token"), true)
        });
        it('make request with correct hash AND signature', async function () {
            let data = {
                "pseudonym" : test_wallet_pseudonym_1,
                "unix_time_ms" : (new Date()).getTime(),
                "signing_key" : wallets[Y].address
            }
            let data_string = JSON.stringify(data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            let signed_hash = await wallets[Y].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/wield_persona", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash
                    }),
                    headers: {
                        Cookie: test_wallet_1_cookie,
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code, "NOW_LOGGED_IN")
        });
    });
    describe('/perform_transaction', async function () {
        it(`mint Wallet 4 aka ${test_wallet_pseudonym_0} some of its own tokens`, async function () {
            let transaction_data = {
                "token_issuer_id_pseudonym" : test_wallet_pseudonym_0,
                "token_issuer_id_public_key": wallets[X].address,
                "from_user_id_pseudonym"    : test_wallet_pseudonym_0,
                "signing_key"               : wallets[X].address,
                "to_user_id_pseudonym"      : test_wallet_pseudonym_0,
                "to_user_id_public_key"     : wallets[X].address,
                "value"                     : 12,
                "transaction_code"          : "MINT",
                "transaction_date"          : (new Date()).getTime(),
                "nonce"                     : 1
            }
            let data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            test_wallet_0_pervious_transaction_hash = calculated_hash
            let signed_hash = await wallets[X].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash
                    }),
                    headers: {
                        Cookie: test_wallet_0_cookie,
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code == "TRANSACTION_SUCCESSFUL", true)
            const balance = await fetch(
                testing_url + "/check_balance", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        token_issuer_id_pseudonym  : test_wallet_pseudonym_0,
                        token_issuer_id_public_key : wallets[X].address,
                        user_id_pseudonym          : test_wallet_pseudonym_0,
                        user_id_public_key         : wallets[X].address
                    }),
                    headers: {
                        Cookie: test_wallet_0_cookie,
                        "content-type": "application/json"
                    }
                }
            );
            let balance_json = await balance.json()
            assert.equal(balance_json.balance, 12)
        })
        it(`mint Wallet 5 aka ${test_wallet_pseudonym_1} some of its own tokens`, async function () {
            let transaction_data = {
                "token_issuer_id_pseudonym" : test_wallet_pseudonym_1,
                "token_issuer_id_public_key": wallets[Y].address,
                "from_user_id_pseudonym"    : test_wallet_pseudonym_1,
                "signing_key"               : wallets[Y].address,
                "to_user_id_pseudonym"      : test_wallet_pseudonym_1,
                "to_user_id_public_key"     : wallets[Y].address,
                "value"                     : 12,
                "transaction_code"          : "MINT",
                "transaction_date"          : (new Date()).getTime(),
                "nonce"                     : 1
            }
            let data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            test_wallet_1_pervious_transaction_hash = calculated_hash
            let signed_hash = await wallets[Y].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash
                    }),
                    headers: {
                        Cookie: test_wallet_1_cookie,
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code == "TRANSACTION_SUCCESSFUL", true)
            const balance = await fetch(
                testing_url + "/check_balance", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        token_issuer_id_pseudonym  : test_wallet_pseudonym_1,
                        token_issuer_id_public_key : wallets[Y].address,
                        user_id_pseudonym          : test_wallet_pseudonym_1,
                        user_id_public_key         : wallets[Y].address
                    }),
                    headers: {
                        Cookie: test_wallet_1_cookie,
                        "content-type": "application/json"
                    }
                }
            );
            let balance_json = await balance.json()
            assert.equal(balance_json.balance, 12)
        })
    })
})