import { ethers } from "ethers";
import fetch from 'node-fetch';
import assert from 'assert'
import { persona_names } from "./persona_names.js"

let special_phase = "curve foster stay broccoli equal icon bamboo champion casino impact will damp"
let wallets = []
for (let i = 0; i < 10; i++) {
  wallets.push(ethers.Wallet.fromMnemonic(special_phase, `m/44'/60'/0'/0/${i}`))
}

let testing_url = "http://localhost:4000"


describe('Testing TRANSFER, REMOVE, and USE_FAUCET transactions', async function () {
    let wallet_3_pseudonym = persona_names[3] // "Edric"
    let wallet_3_access_token = null
    let wallet_3_pervious_transaction_hash = null
    let wallet_4_pseudonym = persona_names[4] // "Irulan"
    let wallet_4_access_token = null
    let wallet_4_pervious_transaction_hash = null
    describe('/api/wield_persona', async function () {
        it(`Send request to get access_token with correct hash AND signature for  ${wallet_3_pseudonym}%${wallets[3].address}`, async function () {
            let data = {
                "pseudonym" : wallet_3_pseudonym,
                "unix_time_ms" : (new Date()).getTime(),
                "signing_key" : wallets[3].address
            }
            let data_string = JSON.stringify(data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            let signed_hash = await wallets[3].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/wield_persona", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            wallet_3_access_token = response_json.data.ACCESS_TOKEN
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code, "NOW_LOGGED_IN")
        });
        it(`Send request to get access_token with correct hash AND signature for  ${wallet_4_pseudonym}%${wallets[4].address}`, async function () {
            let data = {
                "pseudonym" : wallet_4_pseudonym, // Hate
                "unix_time_ms" : (new Date()).getTime(),
                "signing_key" : wallets[4].address
            }
            let data_string = JSON.stringify(data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            let signed_hash = await wallets[4].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/wield_persona", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            wallet_3_access_token = response_json.data.ACCESS_TOKEN
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code, "NOW_LOGGED_IN")
        });
    });
    describe('/api/perform_transaction TRANSFER and REMOVE transactions', async function () {
        it(`Send MINT transaction for ${wallet_3_pseudonym}%${wallets[3].address}`, async function () {
            let transaction_data = {
                "token_issuer_id_pseudonym" : wallet_3_pseudonym,
                "token_issuer_id_public_key": wallets[3].address,
                "from_user_id_pseudonym"    : wallet_3_pseudonym,
                "signing_key"               : wallets[3].address,
                "to_user_id_pseudonym"      : wallet_3_pseudonym,
                "to_user_id_public_key"     : wallets[3].address,
                "value"                     : 12,
                "transaction_code"          : "MINT",
                "transaction_date"          : (new Date()).getTime(),
                "nonce"                     : 1
            }
            let data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            wallet_3_pervious_transaction_hash = calculated_hash
            let signed_hash = await wallets[3].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash,
                        access_token: wallet_3_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code == "TRANSACTION_SUCCESSFUL", true)
            const balance = await fetch(
                testing_url + "/api/check_balance", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        token_issuer_id_pseudonym  : wallet_3_pseudonym,
                        token_issuer_id_public_key : wallets[3].address,
                        user_id_pseudonym          : wallet_3_pseudonym,
                        user_id_public_key         : wallets[3].address,
                        access_token: wallet_3_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let balance_json = await balance.json()
            assert.equal(balance_json.balance, 12)
        });
        it(`Send MINT transaction for ${wallet_4_pseudonym}%${wallets[4].address}`, async function () {
            let transaction_data = {
                "token_issuer_id_pseudonym" : wallet_4_pseudonym,
                "token_issuer_id_public_key": wallets[4].address,
                "from_user_id_pseudonym"    : wallet_4_pseudonym,
                "signing_key"               : wallets[4].address,
                "to_user_id_pseudonym"      : wallet_4_pseudonym,
                "to_user_id_public_key"     : wallets[4].address,
                "value"                     : 12,
                "transaction_code"          : "MINT",
                "transaction_date"          : (new Date()).getTime(),
                "nonce"                     : 1
            }
            let data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            wallet_4_pervious_transaction_hash = calculated_hash
            let signed_hash = await wallets[4].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash,
                        access_token: wallet_4_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code, "TRANSACTION_SUCCESSFUL")
            const balance = await fetch(
                testing_url + "/api/check_balance", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        token_issuer_id_pseudonym  : wallet_4_pseudonym,
                        token_issuer_id_public_key : wallets[4].address,
                        user_id_pseudonym          : wallet_4_pseudonym,
                        user_id_public_key         : wallets[4].address,
                        access_token: wallet_3_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            )
            let balance_json = await balance.json()
            assert.equal(balance_json.balance, 12)
        })
        it(`Send TRANSFER transaction from ${wallet_3_pseudonym}%${wallets[3].address} to ${wallet_4_pseudonym}%${wallets[4].address}`, async function () {
            let transaction_data = {
                "token_issuer_id_pseudonym" : wallet_3_pseudonym,
                "token_issuer_id_public_key": wallets[3].address,
                "from_user_id_pseudonym"    : wallet_3_pseudonym,
                "signing_key"               : wallets[3].address,
                "to_user_id_pseudonym"      : wallet_4_pseudonym,
                "to_user_id_public_key"     : wallets[4].address,
                "value"                     : 4,
                "transaction_code"          : "TRANSFER",
                "transaction_date"          : (new Date()).getTime(),
                "nonce"                     : 2,
                "previous_transaction_hash" : wallet_3_pervious_transaction_hash
            }
            let data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            wallet_3_pervious_transaction_hash = calculated_hash
            let signed_hash = await wallets[3].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash,
                        access_token: wallet_3_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code, "TRANSACTION_SUCCESSFUL")
            const balance_1 = await fetch(
                testing_url + "/api/check_balance", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        token_issuer_id_pseudonym  : wallet_3_pseudonym,
                        token_issuer_id_public_key : wallets[3].address,
                        user_id_pseudonym          : wallet_3_pseudonym,
                        user_id_public_key         : wallets[3].address,
                        access_token: wallet_3_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let balance_1_json = await balance_1.json()
            assert.equal(balance_1_json.balance, 8)
            const balance_2 = await fetch(
                testing_url + "/api/check_balance", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        token_issuer_id_pseudonym  : wallet_3_pseudonym,
                        token_issuer_id_public_key : wallets[3].address,
                        user_id_pseudonym          : wallet_4_pseudonym,
                        user_id_public_key         : wallets[4].address,
                        access_token: wallet_3_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let balance_2_json = await balance_2.json()
            assert.equal(balance_2_json.balance, 4)
        });
        it(`Send REMOVE Transaction for 3 tokens issued by ${wallet_3_pseudonym}%${wallets[3].address} from ${wallet_4_pseudonym}%${wallets[4].address}`, async function () {
            let transaction_data = {
                "token_issuer_id_pseudonym" : wallet_3_pseudonym,
                "token_issuer_id_public_key": wallets[3].address,
                "from_user_id_pseudonym"    : wallet_3_pseudonym,
                "signing_key"               : wallets[3].address,
                "to_user_id_pseudonym"      : wallet_4_pseudonym,
                "to_user_id_public_key"     : wallets[4].address,
                "value"                     : 3,
                "transaction_code"          : "REMOVE",
                "transaction_date"          : (new Date()).getTime(),
                "nonce"                     : 3,
                "previous_transaction_hash" : wallet_3_pervious_transaction_hash
            }
            let data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            wallet_3_pervious_transaction_hash = calculated_hash
            let signed_hash = await wallets[3].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash,
                        access_token: wallet_3_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code, "TRANSACTION_SUCCESSFUL")
            const balance = await fetch(
                testing_url + "/api/check_balance", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        token_issuer_id_pseudonym  : wallet_3_pseudonym,
                        token_issuer_id_public_key : wallets[3].address,
                        user_id_pseudonym          : wallet_4_pseudonym,
                        user_id_public_key         : wallets[4].address,
                        access_token: wallet_3_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let balance_json = await balance.json()
            assert.equal(balance_json.balance, 1)
        })
    })
    describe('/api/perform_transaction USE_FAUCET transaction', async function () {
        it(`${wallet_3_pseudonym}x${wallets[3].address} uses ${wallet_4_pseudonym}x${wallets[4].address}'s Faucet`, async function () {
            let transaction_data = {
                "token_issuer_id_pseudonym" : wallet_3_pseudonym,
                "token_issuer_id_public_key": wallets[3].address,
                "from_user_id_pseudonym"    : wallet_4_pseudonym,
                "signing_key"               : wallets[4].address,
                "to_user_id_pseudonym"      : wallet_4_pseudonym,
                "to_user_id_public_key"     : wallets[4].address,
                "transaction_code"          : "USE_FAUCET",
                "transaction_date"          : (new Date()).getTime(),
                "nonce"                     : 2,
                "previous_transaction_hash" : wallet_4_pervious_transaction_hash
            }
            let data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            wallet_4_pervious_transaction_hash = calculated_hash
            let signed_hash = await wallets[4].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash,
                        access_token: wallet_3_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code, "TRANSACTION_SUCCESSFUL")
            const balance = await fetch(
                testing_url + "/api/check_balance", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        token_issuer_id_pseudonym  : wallet_3_pseudonym,
                        token_issuer_id_public_key : wallets[3].address,
                        user_id_pseudonym          : wallet_4_pseudonym,
                        user_id_public_key         : wallets[4].address,
                        access_token: wallet_3_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let balance_json = await balance.json()
            assert.equal(balance_json.balance, 5)
        });
    })
});