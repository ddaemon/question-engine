import { ethers } from "ethers";
import fetch from 'node-fetch';
import assert from 'assert'
import { persona_names } from "./persona_names.js"
import Persona from "../helper_functions/Persona.js"
import { survey_questions } from "./survey_questions.js";
let special_phase = "curve foster stay broccoli equal icon bamboo champion casino impact will damp"
let wallets = []
for (let i = 0; i < 14; i++) {
  wallets.push(ethers.Wallet.fromMnemonic(special_phase, `m/44'/60'/0'/0/${i}`))
}

let testing_url = "http://localhost:4000"


describe('Testing MEME Transactions', async function () {
    let Persona12 = null
    describe('Ask all the survey questions', async function () {
      it('Persona 12 Mint 144 tokens form themselves', async function () {
        Persona12 = new Persona("EGREGORE", wallets[12].address, "", testing_url)
        let check_persona_exists = await Persona12.lookupPseudonym(Persona12.evm_address)
        assert.equal(check_persona_exists, false)
        Persona12.setPseudonym( persona_names[12] )
        let sign_in_json =  await Persona12.generate_sign_in_hash()
        sign_in_json.signature_of_hash = await wallets[12].signMessage( sign_in_json.hash )
        let sign_in_result = await Persona12.sign_in(  sign_in_json )
        Persona12.setSignedIn(sign_in_result)
        assert.equal(Persona12.access_token != null, true)
        let transaction_data = await Persona12.GENERATE_INITIAL_MINT_TRANSACTION(144)
        transaction_data.signature_of_hash = await wallets[12].signMessage( transaction_data.hash )
        let transaction_result = await Persona12.SEND_TRANSACTION(transaction_data)
        assert.equal("response_type" in transaction_result, true)
        assert.equal(transaction_result.response_type == "SUCCESS", true)
        let token_balance = await Persona12.get_my_token_balance(Persona12.pseudonym, Persona12.evm_address)
        assert.equal(token_balance.data.TOKEN_AMOUNT == 144, true)
      })
      it('Persona12 Loop through the memes minting them', async function () {
        let tmp_persona = Persona12
        let tmp_wallet  = wallets[12]
        assert.equal(tmp_persona.access_token != null, true)
        for(var i = 0; i < survey_questions.length; i++){
          tmp_persona.setPreviousTransaction(  await tmp_persona.get_previous_transaction() )
          let meme_data = {
              "created": (new Date()).getTime(),
              "text": {
                  title: survey_questions[i],
                  body : ""
              },
              "type": "text",
              "meme_type_code": "ROOT_QUESTION",
              "permissions": "PUBLIC"
          }
          let transaction_data = await tmp_persona.GENERATE_MEME_TRANSACTION(
              meme_data,
              tmp_persona.pseudonym,
              tmp_persona.evm_address,
              1
          )
          transaction_data.signature_of_hash = await tmp_wallet.signMessage( transaction_data.hash )
          let transaction_result = await tmp_persona.SEND_TRANSACTION(transaction_data)
          assert.equal("response_type" in transaction_result, true)
          assert.equal(transaction_result.response_type == "SUCCESS", true, "Got Error sending transaction")
        }
        let token_balance = await tmp_persona.get_my_token_balance(tmp_persona.pseudonym, tmp_persona.evm_address)
        assert.equal(token_balance.data.TOKEN_AMOUNT == 144 - survey_questions.length, true)
    })
    })
})