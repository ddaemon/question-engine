import { ethers } from "ethers";
import fetch from 'node-fetch';
import assert from 'assert'
import { persona_names } from "./persona_names.js"

let special_phase = "curve foster stay broccoli equal icon bamboo champion casino impact will damp"
let wallets = []
for (let i = 0; i < 10; i++) {
  wallets.push(ethers.Wallet.fromMnemonic(special_phase, `m/44'/60'/0'/0/${i}`))
}

let testing_url = "http://localhost:4000"


describe('Testing MINT and BURN transactions', async function () {
    let cookie = null
    let wallet_0_pseudonym = persona_names[0] // "Hayt"
    let previous_transaction_hash = null
    describe('/api/wield_persona Test logging in', async function () {
        it('Send request to get access_token with incorrect JSON keys', async function () {
            let data = {
                "message" : "God cares about you"
            }
            let data_string = JSON.stringify(data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes("test"))
            const response = await fetch(
                testing_url + "/api/wield_persona", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data2 : data_string,
                        hash : calculated_hash,
                        signature_of_hash: "TODO"
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "ERROR", true)
            assert.equal(response_json.response_code, "INVALID_JSON")
        });
        it('Send request to get access_token with wrong hash of data', async function () {
            let data = {
                "message" : "God cares about you"
            }
            let data_string = JSON.stringify(data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes("test"))
            const response = await fetch(
                testing_url + "/api/wield_persona", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: "TODO"
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "ERROR", true)
            assert.equal(response_json.response_code, "INCORRECT_HASH_OF_DATA")
        });
        it('Send request to get access_token with correct hash but wrong signature', async function () {
            let data = {
                "message" : "God cares about you"
            }
            let data_string = JSON.stringify(data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            const response = await fetch(
                testing_url + "/api/wield_persona", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: "TODO"
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "ERROR", true)
            assert.equal(response_json.response_code, "NO_SIGNING_KEY")
        });
        it('Send request to get access_token with an access_token', async function () {
            let data = {
                "pseudonym" : wallet_0_pseudonym, // Hate
                "unix_time_ms" : (new Date()).getTime(),
                "signing_key" : wallets[0].address
            }
            let data_string = JSON.stringify(data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            let signed_hash = await wallets[0].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/wield_persona", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash,
                        access_token : "Wrong Cookie"
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "ERROR", true)
            assert.equal(response_json.response_code, "INVALID_JSON")
        });
        it('Send request to get access_token with correct hash AND signature', async function () {
            let data = {
                "pseudonym" : wallet_0_pseudonym, // Hate
                "unix_time_ms" : (new Date()).getTime(),
                "signing_key" : wallets[0].address
            }
            let data_string = JSON.stringify(data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            let signed_hash = await wallets[0].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/wield_persona", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            cookie = response_json.data.ACCESS_TOKEN
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code, "NOW_LOGGED_IN")
        });
        it('Send request to get access_token with correct hash AND signature when issued access_token (Check #TODO)', async function () {
            // #TODO Right now we issue as many access_tokens as a user requests, we need code to limit this or manage them
            let data = {
                "pseudonym" : "Hote",
                "unix_time_ms" : (new Date()).getTime(),
                "signing_key" : wallets[0].address
            }
            let data_string = JSON.stringify(data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            let signed_hash = await wallets[0].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/wield_persona", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash,
                        access_token : cookie
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "ERROR", true)
            assert.equal(response_json.response_code, "INVALID_JSON")
        });
    });
    describe('/api/perform_transaction MINT', async function () {
        it('Send MINT transaction with invalid data', async function () {
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes("I Like Pie"))
            let signed_hash = await wallets[0].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : "I Like Pie",
                        hash : calculated_hash,
                        signature_of_hash: signed_hash,
                        access_token : cookie
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "ERROR", true)
            assert.equal(response_json.response_code, "INVALID_DATA_JSON")
        });
        it('Send MINT transaction with data hash', async function () {
            let transaction_data = {
                "token_issuer_id_pseudonym" : wallet_0_pseudonym,
                "token_issuer_id_public_key": wallets[0].address,
                "from_user_id_pseudonym"    : wallet_0_pseudonym,
                "from_user_id_public_key"   : wallets[0].address,
                "to_user_id_pseudonym"      : wallet_0_pseudonym,
                "to_user_id_public_key"     : wallets[0].address,
                "value"                     : 12,
                "transaction_code"          : "MINT"
            }
            let data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes("I Like Pie!"))
            let signed_hash = await wallets[0].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash,
                        access_token : cookie
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "ERROR", true)
            assert.equal(response_json.response_code, "INCORRECT_HASH_OF_DATA")
        });
        it('Send MINT transaction that is valid', async function () {
            let transaction_data = {
                "token_issuer_id_pseudonym" : wallet_0_pseudonym,
                "token_issuer_id_public_key": wallets[0].address,
                "from_user_id_pseudonym"    : wallet_0_pseudonym,
                "signing_key"               : wallets[0].address,
                "to_user_id_pseudonym"      : wallet_0_pseudonym,
                "to_user_id_public_key"     : wallets[0].address,
                "value"                     : 12,
                "transaction_code"          : "MINT",
                "transaction_date"          : (new Date()).getTime(),
                "nonce"                     : 1
            }
            let data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            previous_transaction_hash = calculated_hash
            let signed_hash = await wallets[0].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code == "TRANSACTION_SUCCESSFUL", true)
        });
        it('Send second MINT transaction with INVALID nonce', async function () {
            let transaction_data = {
                "token_issuer_id_pseudonym" : wallet_0_pseudonym,
                "token_issuer_id_public_key": wallets[0].address,
                "from_user_id_pseudonym"    : wallet_0_pseudonym,
                "signing_key"               : wallets[0].address,
                "to_user_id_pseudonym"      : wallet_0_pseudonym,
                "to_user_id_public_key"     : wallets[0].address,
                "value"                     : 12,
                "transaction_code"          : "MINT",
                "transaction_date"          : (new Date()).getTime(),
                "nonce"                     : 1
            }
            let data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            let signed_hash = await wallets[0].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash,
                        access_token : cookie
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "ERROR", true)
            assert.equal(response_json.response_code, "INVALID_TRANSACTION")
        });
        it('Send second MINT transaction with CORRECT nonce', async function () {
            let transaction_data = {
                "token_issuer_id_pseudonym" : wallet_0_pseudonym,
                "token_issuer_id_public_key": wallets[0].address,
                "from_user_id_pseudonym"    : wallet_0_pseudonym,
                "signing_key"               : wallets[0].address,
                "to_user_id_pseudonym"      : wallet_0_pseudonym,
                "to_user_id_public_key"     : wallets[0].address,
                "value"                     : 12,
                "transaction_code"          : "MINT",
                "transaction_date"          : (new Date()).getTime(),
                "nonce"                     : 2,
                "previous_transaction_hash" : previous_transaction_hash
            }
            let data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            previous_transaction_hash = calculated_hash
            let signed_hash = await wallets[0].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash,
                        access_token : cookie
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            const balance = await fetch(
                testing_url + "/api/check_balance", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        token_issuer_id_pseudonym  : wallet_0_pseudonym,
                        token_issuer_id_public_key : wallets[0].address,
                        user_id_pseudonym          : wallet_0_pseudonym,
                        user_id_public_key         : wallets[0].address,
                        access_token : cookie
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let balance_json = await balance.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code, "TRANSACTION_SUCCESSFUL")
            assert.equal(balance_json.balance, 24)
        })
    })
    describe('/api/perform_transaction BURN', async function () {
        it('Send BURN transaction #TODO, validate balance', async function () {
            let transaction_data = {
                "token_issuer_id_pseudonym" : wallet_0_pseudonym,
                "token_issuer_id_public_key": wallets[0].address,
                "from_user_id_pseudonym"    : wallet_0_pseudonym,
                "signing_key"               : wallets[0].address,
                // "to_user_id_pseudonym"      : wallet_4_pseudonym,
                // "to_user_id_public_key"     : wallets[4].address,
                "valuea"                     : 12,
                "transaction_code"          : "BURN",
                "transaction_date"          : (new Date()).getTime(),
                "nonce"                     : 3,
                "previous_transaction_hash" : previous_transaction_hash
            }
            let data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            let signed_hash = await wallets[0].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash,
                        access_token : cookie
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "ERROR", true)
            assert.equal(response_json.response_code, "INVALID_DATA_JSON")
        });
        it('Send BURN transaction for tokens that do not exist', async function () {
            let transaction_data = {
                "token_issuer_id_pseudonym" : wallet_0_pseudonym,
                "token_issuer_id_public_key": wallets[1].address, // FYI This one was changed
                "from_user_id_pseudonym"    : wallet_0_pseudonym,
                "signing_key"               : wallets[0].address,
                "value"                     : 12,
                "transaction_code"          : "BURN",
                "transaction_date"          : (new Date()).getTime(),
                "nonce"                     : 3,
                "previous_transaction_hash" : previous_transaction_hash
            }
            let data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            let signed_hash = await wallets[0].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash,
                        access_token : cookie
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "ERROR", true)
            assert.equal(response_json.response_code, "INVALID_DATA_JSON")
        });
        it('Send BURN transaction for more tokens than Persona owns', async function () {
            let transaction_data = {
                "token_issuer_id_pseudonym" : wallet_0_pseudonym,
                "token_issuer_id_public_key": wallets[0].address,
                "from_user_id_pseudonym"    : wallet_0_pseudonym,
                "signing_key"               : wallets[0].address,
                "value"                     : 50,
                "transaction_code"          : "BURN",
                "transaction_date"          : (new Date()).getTime(),
                "nonce"                     : 3,
                "previous_transaction_hash" : previous_transaction_hash
            }
            let data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            let signed_hash = await wallets[0].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash,
                        access_token : cookie
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "ERROR", true)
            assert.equal(response_json.response_code, "INVALID_DATA_JSON")
        });
        it('Send BURN transaction to burn tokens same Persona issued', async function () {
            let transaction_data = {
                "token_issuer_id_pseudonym" : wallet_0_pseudonym,
                "token_issuer_id_public_key": wallets[0].address,
                "from_user_id_pseudonym"    : wallet_0_pseudonym,
                "signing_key"               : wallets[0].address,
                'to_user_id_pseudonym'      : "Zero",
                'to_user_id_public_key'     : '0x0000000000000000000000000000000000000000',
                "value"                     : 3,
                "transaction_code"          : "BURN",
                "transaction_date"          : (new Date()).getTime(),
                "nonce"                     : 3,
                "previous_transaction_hash" : previous_transaction_hash
            }
            let data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            let signed_hash = await wallets[0].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash,
                        access_token : cookie
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code, "TRANSACTION_SUCCESSFUL")
        });
    })
});