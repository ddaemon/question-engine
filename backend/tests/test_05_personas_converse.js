import { ethers } from "ethers";
import fetch from 'node-fetch';
import assert from 'assert'
import { persona_names } from "./persona_names.js"
import Persona from "../helper_functions/Persona.js"

let special_phase = "curve foster stay broccoli equal icon bamboo champion casino impact will damp"
let wallets = []
for (let i = 0; i < 10; i++) {
  wallets.push(ethers.Wallet.fromMnemonic(special_phase, `m/44'/60'/0'/0/${i}`))
}

let testing_url = "http://localhost:4000"


describe('Testing MEME Transactions', async function () {
    let Persona1 = null
    let Persona8 = null
    let Persona9 = null
    describe('Have the three test Peronas mint 24 of their token', async function () {
        it('Create Persona1 and MINT 24 tokens', async function () {
            Persona1 = new Persona("EGREGORE", wallets[1].address, "", testing_url)
            let check_persona_exists = await Persona1.lookupPseudonym(Persona1.evm_address)
            assert.equal(check_persona_exists, false)
            Persona1.setPseudonym( persona_names[1] )
            let sign_in_json =  await Persona1.generate_sign_in_hash()
            sign_in_json.signature_of_hash = await wallets[1].signMessage( sign_in_json.hash )
            let sign_in_result = await Persona1.sign_in(  sign_in_json )
            Persona1.setSignedIn(sign_in_result)
            assert.equal(Persona1.access_token != null, true)
            let transaction_data = await Persona1.GENERATE_INITIAL_MINT_TRANSACTION(24)
            transaction_data.signature_of_hash = await wallets[1].signMessage( transaction_data.hash )
            let transaction_result = await Persona1.SEND_TRANSACTION(transaction_data)
            assert.equal("response_type" in transaction_result, true)
            assert.equal(transaction_result.response_type == "SUCCESS", true)
            let token_balance = await Persona1.get_my_token_balance(Persona1.pseudonym, Persona1.evm_address)
            assert.equal(token_balance.data.TOKEN_AMOUNT == 24, true)
        })
        it('Create Persona8 and MINT 24 tokens', async function () {
            Persona8 = new Persona("EGREGORE", wallets[8].address, "", testing_url)
            let check_persona_exists = await Persona8.lookupPseudonym(Persona8.evm_address)
            assert.equal(check_persona_exists, false)
            Persona8.setPseudonym( persona_names[8] )
            let sign_in_json =  await Persona8.generate_sign_in_hash()
            sign_in_json.signature_of_hash = await wallets[8].signMessage( sign_in_json.hash )
            let sign_in_result = await Persona8.sign_in(  sign_in_json )
            Persona8.setSignedIn(sign_in_result)
            assert.equal(Persona8.access_token != null, true)
            let transaction_data = await Persona8.GENERATE_INITIAL_MINT_TRANSACTION(24)
            transaction_data.signature_of_hash = await wallets[8].signMessage( transaction_data.hash )
            let transaction_result = await Persona8.SEND_TRANSACTION(transaction_data)
            assert.equal("response_type" in transaction_result, true)
            assert.equal(transaction_result.response_type == "SUCCESS", true)
            let token_balance = await Persona8.get_my_token_balance(Persona8.pseudonym, Persona8.evm_address)
            assert.equal(token_balance.data.TOKEN_AMOUNT == 24, true)
        })
        it('Create Persona9 and MINT 24 tokens', async function () {
            Persona9 = new Persona("EGREGORE", wallets[9].address, "", testing_url)
            let check_persona_exists = await Persona9.lookupPseudonym(Persona9.evm_address)
            assert.equal(check_persona_exists, false)
            Persona9.setPseudonym( persona_names[9] )
            let sign_in_json =  await Persona9.generate_sign_in_hash()
            sign_in_json.signature_of_hash = await wallets[9].signMessage( sign_in_json.hash )
            let sign_in_result = await Persona9.sign_in(  sign_in_json )
            Persona9.setSignedIn(sign_in_result)
            assert.equal(Persona9.access_token != null, true)
            let transaction_data = await Persona9.GENERATE_INITIAL_MINT_TRANSACTION(24)
            transaction_data.signature_of_hash = await wallets[9].signMessage( transaction_data.hash )
            let transaction_result = await Persona9.SEND_TRANSACTION(transaction_data)
            assert.equal("response_type" in transaction_result, true)
            assert.equal(transaction_result.response_type == "SUCCESS", true)
            let token_balance = await Persona9.get_my_token_balance(Persona9.pseudonym, Persona9.evm_address)
            assert.equal(token_balance.data.TOKEN_AMOUNT == 24, true)
        })
    })
    describe('Everyone asks two questions', async function () {
        it('Persona1 Mints a meme', async function () {
            let tmp_persona = Persona1
            let tmp_wallet  = wallets[1]
            assert.equal(tmp_persona.access_token != null, true)
            tmp_persona.setPreviousTransaction(  await tmp_persona.get_previous_transaction() )
            let meme_data = {
                "created": (new Date()).getTime(),
                "text": {
                    title: "Is what our companies produce actually worth anything?",
                    body : ""
                },
                "type": "text",
                "meme_type_code": "ROOT_QUESTION",
                "permissions": "PUBLIC"
            }
            let transaction_data = await tmp_persona.GENERATE_MEME_TRANSACTION(
                meme_data,
                tmp_persona.pseudonym,
                tmp_persona.evm_address,
                1
            )
            transaction_data.signature_of_hash = await tmp_wallet.signMessage( transaction_data.hash )
            let transaction_result = await tmp_persona.SEND_TRANSACTION(transaction_data)
            assert.equal("response_type" in transaction_result, true)
            assert.equal(transaction_result.response_type == "SUCCESS", true)
            let token_balance = await tmp_persona.get_my_token_balance(tmp_persona.pseudonym, tmp_persona.evm_address)
            assert.equal(token_balance.data.TOKEN_AMOUNT == 23, true)
        })
        it('Persona1 Mints a second meme', async function () {
            let tmp_persona = Persona1
            let tmp_wallet  = wallets[1]
            assert.equal(tmp_persona.access_token != null, true)
            tmp_persona.setPreviousTransaction(  await tmp_persona.get_previous_transaction() )
            let meme_data = {
                "created": (new Date()).getTime(),
                "text": {
                    title: "If there were a set of social norms for how your friends interact with you what would they be?",
                    body : ""
                },
                "type": "text",
                "meme_type_code": "ROOT_QUESTION",
                "permissions": "PUBLIC"
            }
            let transaction_data = await tmp_persona.GENERATE_MEME_TRANSACTION(
                meme_data,
                tmp_persona.pseudonym,
                tmp_persona.evm_address,
                1
            )
            transaction_data.signature_of_hash = await tmp_wallet.signMessage( transaction_data.hash )
            let transaction_result = await tmp_persona.SEND_TRANSACTION(transaction_data)
            assert.equal("response_type" in transaction_result, true)
            assert.equal(transaction_result.response_type == "SUCCESS", true)
            let token_balance = await tmp_persona.get_my_token_balance(tmp_persona.pseudonym, tmp_persona.evm_address)
            assert.equal(token_balance.data.TOKEN_AMOUNT == 22, true)
        })
        it('Persona8 Mints a meme', async function () {
            let tmp_persona = Persona8
            let tmp_wallet  = wallets[8]
            assert.equal(tmp_persona.access_token != null, true)
            tmp_persona.setPreviousTransaction(  await tmp_persona.get_previous_transaction() )
            let meme_data = {
                "created": (new Date()).getTime(),
                "text": {
                    title: "Can we measure how people diverge from personality?",
                    body : ""
                },
                "type": "text",
                "meme_type_code": "ROOT_QUESTION",
                "permissions": "PUBLIC"
            }
            let transaction_data = await tmp_persona.GENERATE_MEME_TRANSACTION(
                meme_data,
                tmp_persona.pseudonym,
                tmp_persona.evm_address,
                1
            )
            transaction_data.signature_of_hash = await tmp_wallet.signMessage( transaction_data.hash )
            let transaction_result = await tmp_persona.SEND_TRANSACTION(transaction_data)
            assert.equal("response_type" in transaction_result, true)
            assert.equal(transaction_result.response_type == "SUCCESS", true)
            let token_balance = await tmp_persona.get_my_token_balance(tmp_persona.pseudonym, tmp_persona.evm_address)
            assert.equal(token_balance.data.TOKEN_AMOUNT == 23, true)
        })
        it('Persona8 Mints a second meme', async function () {
            let tmp_persona = Persona8
            let tmp_wallet  = wallets[8]
            assert.equal(tmp_persona.access_token != null, true)
            tmp_persona.setPreviousTransaction(  await tmp_persona.get_previous_transaction() )
            let meme_data = {
                "created": (new Date()).getTime(),
                "text": {
                    title: "Why can't I bear to look at what I have done and said in the past?",
                    body : ""
                },
                "type": "text",
                "meme_type_code": "ROOT_QUESTION",
                "permissions": "PUBLIC"
            }
            let transaction_data = await tmp_persona.GENERATE_MEME_TRANSACTION(
                meme_data,
                tmp_persona.pseudonym,
                tmp_persona.evm_address,
                1
            )
            transaction_data.signature_of_hash = await tmp_wallet.signMessage( transaction_data.hash )
            let transaction_result = await tmp_persona.SEND_TRANSACTION(transaction_data)
            assert.equal("response_type" in transaction_result, true)
            assert.equal(transaction_result.response_type == "SUCCESS", true)
            let token_balance = await tmp_persona.get_my_token_balance(tmp_persona.pseudonym, tmp_persona.evm_address)
            assert.equal(token_balance.data.TOKEN_AMOUNT == 22, true)
        })
        it('Persona9 Mints a meme', async function () {
            let tmp_persona = Persona9
            let tmp_wallet  = wallets[9]
            assert.equal(tmp_persona.access_token != null, true)
            tmp_persona.setPreviousTransaction(  await tmp_persona.get_previous_transaction() )
            let meme_data = {
                "created": (new Date()).getTime(),
                "text": {
                    title: "What would the opposite of a degenerate look like?",
                    body : ""
                },
                "type": "text",
                "meme_type_code": "ROOT_QUESTION",
                "permissions": "PUBLIC"
            }
            let transaction_data = await tmp_persona.GENERATE_MEME_TRANSACTION(
                meme_data,
                tmp_persona.pseudonym,
                tmp_persona.evm_address,
                1
            )
            transaction_data.signature_of_hash = await tmp_wallet.signMessage( transaction_data.hash )
            let transaction_result = await tmp_persona.SEND_TRANSACTION(transaction_data)
            assert.equal("response_type" in transaction_result, true)
            assert.equal(transaction_result.response_type == "SUCCESS", true)
            let token_balance = await tmp_persona.get_my_token_balance(tmp_persona.pseudonym, tmp_persona.evm_address)
            assert.equal(token_balance.data.TOKEN_AMOUNT == 23, true)
        })
        it('Persona9 Mints a second meme', async function () {
            let tmp_persona = Persona9
            let tmp_wallet  = wallets[9]
            assert.equal(tmp_persona.access_token != null, true)
            tmp_persona.setPreviousTransaction(  await tmp_persona.get_previous_transaction() )
            let meme_data = {
                "created": (new Date()).getTime(),
                "text": {
                    title: "What does it mean to watch the watchers?",
                    body : ""
                },
                "type": "text",
                "meme_type_code": "ROOT_QUESTION",
                "permissions": "PUBLIC"
            }
            let transaction_data = await tmp_persona.GENERATE_MEME_TRANSACTION(
                meme_data,
                tmp_persona.pseudonym,
                tmp_persona.evm_address,
                1
            )
            transaction_data.signature_of_hash = await tmp_wallet.signMessage( transaction_data.hash )
            let transaction_result = await tmp_persona.SEND_TRANSACTION(transaction_data)
            assert.equal("response_type" in transaction_result, true)
            assert.equal(transaction_result.response_type == "SUCCESS", true)
            let token_balance = await tmp_persona.get_my_token_balance(tmp_persona.pseudonym, tmp_persona.evm_address)
            assert.equal(token_balance.data.TOKEN_AMOUNT == 22, true)
        })
    })
})