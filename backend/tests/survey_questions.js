export const survey_questions = [
  "Do you have a centralized place for your online personas like linktree or a personal website?", // 0
  "Do you think having a personal site that is useful?", // 1
  "What is your favorite part of the internet?",
  "What websites or platforms do you spend time on?",
  "What feelings and experiences do you get from each community that keep you coming back?",
  "Where do you assert your will online?",
  "What do you get out of lurking, consuming, the internet?",
  "What do you like about group chats?",
  "What do you dislike about group chats?",
  "What does the internet need less of?",
  "What does the internet need more of?",
  "Do you want more control over your Social Media feed?",
  "How would you query your friends public social media?",
  "How would you query your friends private daemon data given they need to approve your queries?"
]