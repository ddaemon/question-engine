import { ethers } from "ethers";
import fetch from 'node-fetch';
import assert from 'assert'
import { persona_names } from "./persona_names.js"

let special_phase = "curve foster stay broccoli equal icon bamboo champion casino impact will damp"
let wallets = []
for (let i = 0; i < 10; i++) {
  wallets.push(ethers.Wallet.fromMnemonic(special_phase, `m/44'/60'/0'/0/${i}`))
}

let testing_url = "http://localhost:4000"


describe('Testing MEME Transactions', async function () {
    let test_wallet_pseudonym_5 = persona_names[5] // Scytale
    let test_wallet_5_access_token = null
    let test_wallet_5_pervious_transaction_hash = null
    let test_wallet_pseudonym_6 = persona_names[6] // Otheym
    let test_wallet_6_access_token = null
    let test_wallet_6_pervious_transaction_hash = null
    let root_question_response = null
    describe('/api/wield_persona', async function () {
        it(`Send request to get access_token with correct hash AND signature for  ${test_wallet_pseudonym_5}%${wallets[5].address}`, async function () {
            let data = {
                "pseudonym" : test_wallet_pseudonym_5,
                "unix_time_ms" : (new Date()).getTime(),
                "signing_key" : wallets[5].address
            }
            let data_string = JSON.stringify(data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            let signed_hash = await wallets[5].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/wield_persona", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            test_wallet_5_access_token = response_json.data.ACCESS_TOKEN
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code, "NOW_LOGGED_IN")
        });
        it(`Send request to get access_token with correct hash AND signature for  ${test_wallet_pseudonym_6}%${wallets[6].address}`, async function () {
            let data = {
                "pseudonym" : test_wallet_pseudonym_6,
                "unix_time_ms" : (new Date()).getTime(),
                "signing_key" : wallets[6].address
            }
            let data_string = JSON.stringify(data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            let signed_hash = await wallets[6].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/wield_persona", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            test_wallet_6_access_token = response_json.data.ACCESS_TOKEN
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code, "NOW_LOGGED_IN")
        });
    });
    describe('/api/perform_transaction MINT', async function () {
        it(`Send VALID MINT transaction for ${test_wallet_pseudonym_5}%${wallets[5].address}`, async function () {
            let transaction_data = {
                "token_issuer_id_pseudonym" : test_wallet_pseudonym_5,
                "token_issuer_id_public_key": wallets[5].address,
                "from_user_id_pseudonym"    : test_wallet_pseudonym_5,
                "signing_key"               : wallets[5].address,
                "to_user_id_pseudonym"      : test_wallet_pseudonym_5,
                "to_user_id_public_key"     : wallets[5].address,
                "value"                     : 12,
                "transaction_code"          : "MINT",
                "transaction_date"          : (new Date()).getTime(),
                "nonce"                     : 1
            }
            let data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            test_wallet_5_pervious_transaction_hash = calculated_hash
            let signed_hash = await wallets[5].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash,
                        access_token: test_wallet_5_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code == "TRANSACTION_SUCCESSFUL", true)
            const balance = await fetch(
                testing_url + "/api/check_balance", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        token_issuer_id_pseudonym  : test_wallet_pseudonym_5,
                        token_issuer_id_public_key : wallets[5].address,
                        user_id_pseudonym          : test_wallet_pseudonym_5,
                        user_id_public_key         : wallets[5].address,
                        access_token: test_wallet_5_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let balance_json = await balance.json()
            assert.equal(balance_json.balance, 12)
        })
        it(`Send VALID MINT transaction for ${test_wallet_pseudonym_6}%${wallets[6].address}`, async function () {
            let transaction_data = {
                "token_issuer_id_pseudonym" : test_wallet_pseudonym_6,
                "token_issuer_id_public_key": wallets[6].address,
                "from_user_id_pseudonym"    : test_wallet_pseudonym_6,
                "signing_key"               : wallets[6].address,
                "to_user_id_pseudonym"      : test_wallet_pseudonym_6,
                "to_user_id_public_key"     : wallets[6].address,
                "value"                     : 12,
                "transaction_code"          : "MINT",
                "transaction_date"          : (new Date()).getTime(),
                "nonce"                     : 1
            }
            let data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            test_wallet_6_pervious_transaction_hash = calculated_hash
            let signed_hash = await wallets[6].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash,
                        access_token: test_wallet_6_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code == "TRANSACTION_SUCCESSFUL", true)
            const balance = await fetch(
                testing_url + "/api/check_balance", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        token_issuer_id_pseudonym  : test_wallet_pseudonym_6,
                        token_issuer_id_public_key : wallets[6].address,
                        user_id_pseudonym          : test_wallet_pseudonym_6,
                        user_id_public_key         : wallets[6].address,
                        access_token: test_wallet_6_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let balance_json = await balance.json()
            assert.equal(balance_json.balance, 12)
        })
    })
    describe('/api/perform_transaction MEME', async function () {
        it(`Send VALID MEME Transaction of type ROOT_QUESTION for ${test_wallet_pseudonym_5}%${wallets[5].address} with PUBLIC permissions`, async function () {
            let meme_data = {
                "created": (new Date()).getTime(),
                "text": {
                    title: "How do you describe yourself?",
                    body : ""
                },
                "type": "text",
                "meme_type_code": "ROOT_QUESTION",
                "permissions": "PUBLIC"
            }
            let meme_data_string = JSON.stringify(meme_data)
            let meme_data_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(meme_data_string))
            let transaction_data = {
                "token_issuer_id_pseudonym" : test_wallet_pseudonym_5,
                "token_issuer_id_public_key": wallets[5].address,
                "from_user_id_pseudonym"    : test_wallet_pseudonym_5,
                "signing_key"               : wallets[5].address,
                "to_user_id_pseudonym"      : "Zero",
                "to_user_id_public_key"     : "0x0000000000000000000000000000000000000000",
                "value"                     : 1,
                "transaction_code"          : "MEME",
                "transaction_date"          : (new Date()).getTime(),
                "nonce"                     : 2,
                "previous_transaction_hash" : test_wallet_5_pervious_transaction_hash,
                "data_hash"                 : meme_data_hash,
            }
            let transaction_data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(transaction_data_string))
            test_wallet_5_pervious_transaction_hash = calculated_hash
            let signed_hash = await wallets[5].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        internal_data     : meme_data_string,
                        data              : transaction_data_string,
                        hash              : calculated_hash,
                        signature_of_hash : signed_hash,
                        access_token: test_wallet_5_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true, "no response_type")
            assert.equal(response_json.response_type == "SUCCESS", true, "response_type should be Should be SUCCESS")
            assert.equal(response_json.response_code == "TRANSACTION_SUCCESSFUL", true, "Wrong response_code")
            const balance = await fetch(
                testing_url + "/api/check_balance", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        token_issuer_id_pseudonym  : test_wallet_pseudonym_5,
                        token_issuer_id_public_key : wallets[5].address,
                        user_id_pseudonym          : test_wallet_pseudonym_5,
                        user_id_public_key         : wallets[5].address,
                        access_token: test_wallet_5_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let balance_json = await balance.json()
            assert.equal(balance_json.balance, 11)
        })
        it(`Send VALID MEME Transaction of type STATEMENT for ${test_wallet_pseudonym_5}%${wallets[5].address} to ROOT_QUESTION above with PUBLIC permissions`, async function () {
            let meme_data = {
                "created": (new Date()).getTime(),
                "text": {
                    title: "I art Randy",
                    body : "PKMS(Personal Knowledge Management System) extremist with an interest in esoteric knowledge. The Kybalion, Soma, Gnosticism, Occultism, Alchemy, Stoicism are all my thing. It is my quest to look for the hidden meanings within ancient and modern media."
                },
                "type": "text",
                "meme_type_code": "STATEMENT",
                "permissions": "PUBLIC",
                "root_meme"  : test_wallet_5_pervious_transaction_hash
            }
            let meme_data_string = JSON.stringify(meme_data)
            let meme_data_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(meme_data_string))
            let transaction_data = {
                "token_issuer_id_pseudonym" : test_wallet_pseudonym_5,
                "token_issuer_id_public_key": wallets[5].address,
                "from_user_id_pseudonym"    : test_wallet_pseudonym_5,
                "signing_key"               : wallets[5].address,
                "to_user_id_pseudonym"      : "Zero",
                "to_user_id_public_key"     : "0x0000000000000000000000000000000000000000",
                "value"                     : 1,
                "transaction_code"          : "MEME",
                "transaction_date"          : (new Date()).getTime(),
                "nonce"                     : 3,
                "data_hash"                 : meme_data_hash,
                "previous_transaction_hash" : test_wallet_5_pervious_transaction_hash
            }
            let transaction_data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(transaction_data_string))
            test_wallet_5_pervious_transaction_hash = calculated_hash
            let signed_hash = await wallets[5].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        internal_data     : meme_data_string,
                        data              : transaction_data_string,
                        hash              : calculated_hash,
                        signature_of_hash : signed_hash,
                        access_token      : test_wallet_5_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code == "TRANSACTION_SUCCESSFUL", true)
            const balance = await fetch(
                testing_url + "/api/check_balance", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        token_issuer_id_pseudonym  : test_wallet_pseudonym_5,
                        token_issuer_id_public_key : wallets[5].address,
                        user_id_pseudonym          : test_wallet_pseudonym_5,
                        user_id_public_key         : wallets[5].address,
                        access_token: test_wallet_5_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let balance_json = await balance.json()
            assert.equal(balance_json.balance, 10)
        })
        it(`Send second VALID MEME Transaction of type ROOT_QUESTION for ${test_wallet_pseudonym_5}%${wallets[5].address} with PUBLIC permissions`, async function () {
            let meme_data = {
                "created": (new Date()).getTime(),
                "text": {
                    title: "What do you do for work?",
                    body : ""
                },
                "type": "text",
                "meme_type_code": "ROOT_QUESTION",
                "permissions": "PUBLIC"
            }
            let meme_data_string = JSON.stringify(meme_data)
            let meme_data_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(meme_data_string))
            let transaction_data = {
                "token_issuer_id_pseudonym" : test_wallet_pseudonym_5,
                "token_issuer_id_public_key": wallets[5].address,
                "from_user_id_pseudonym"    : test_wallet_pseudonym_5,
                "signing_key"               : wallets[5].address,
                "to_user_id_pseudonym"      : "Zero",
                "to_user_id_public_key"     : "0x0000000000000000000000000000000000000000",
                "value"                     : 1,
                "transaction_code"          : "MEME",
                "transaction_date"          : (new Date()).getTime(),
                "nonce"                     : 4,
                "data_hash"                 : meme_data_hash,
                "previous_transaction_hash" : test_wallet_5_pervious_transaction_hash
            }
            let transaction_data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(transaction_data_string))
            test_wallet_5_pervious_transaction_hash = calculated_hash
            root_question_response = test_wallet_5_pervious_transaction_hash
            let signed_hash = await wallets[5].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        internal_data     : meme_data_string,
                        data              : transaction_data_string,
                        hash              : calculated_hash,
                        signature_of_hash : signed_hash,
                        access_token: test_wallet_5_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code == "TRANSACTION_SUCCESSFUL", true)
            const balance = await fetch(
                testing_url + "/api/check_balance", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        token_issuer_id_pseudonym  : test_wallet_pseudonym_5,
                        token_issuer_id_public_key : wallets[5].address,
                        user_id_pseudonym          : test_wallet_pseudonym_5,
                        user_id_public_key         : wallets[5].address,
                        access_token: test_wallet_5_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let balance_json = await balance.json()
            assert.equal(balance_json.balance, 9)
        })
        it(`Send second VALID MEME Transaction of type STATEMENT for ${test_wallet_pseudonym_5}%${wallets[5].address} with REQUIRE_PAYMENT_RESPONDER permissions`, async function () {
            let meme_data = {
                "created": (new Date()).getTime(),
                "text": {
                    title: "What I (Randy) do for work",
                    body : "I work as a SysAdmin. That means I log into different servers and make sure their services are up and running. I perform backups. I add and remove machines from the different kuberneties cluster I manage for the company I work for."
                },
                "type": "text",
                "meme_type_code": "STATEMENT",
                "permissions": "REQUIRE_PAYMENT_RESPONDER",
                "root_meme"  : test_wallet_5_pervious_transaction_hash
            }
            let meme_data_string = JSON.stringify(meme_data)
            let meme_data_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(meme_data_string))
            let transaction_data = {
                "token_issuer_id_pseudonym" : test_wallet_pseudonym_5,
                "token_issuer_id_public_key": wallets[5].address,
                "from_user_id_pseudonym"    : test_wallet_pseudonym_5,
                "signing_key"               : wallets[5].address,
                "to_user_id_pseudonym"      : "Zero",
                "to_user_id_public_key"     : "0x0000000000000000000000000000000000000000",
                "value"                     : 1,
                "transaction_code"          : "MEME",
                "transaction_date"          : (new Date()).getTime(),
                "nonce"                     : 5,
                "data_hash"                 : meme_data_hash,
                "previous_transaction_hash" : test_wallet_5_pervious_transaction_hash
            }
            let transaction_data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(transaction_data_string))
            test_wallet_5_pervious_transaction_hash = calculated_hash
            let signed_hash = await wallets[5].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        internal_data     : meme_data_string,
                        data              : transaction_data_string,
                        hash              : calculated_hash,
                        signature_of_hash : signed_hash,
                        access_token: test_wallet_5_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code == "TRANSACTION_SUCCESSFUL", true)
            const balance = await fetch(
                testing_url + "/api/check_balance", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        token_issuer_id_pseudonym  : test_wallet_pseudonym_5,
                        token_issuer_id_public_key : wallets[5].address,
                        user_id_pseudonym          : test_wallet_pseudonym_5,
                        user_id_public_key         : wallets[5].address,
                        access_token: test_wallet_5_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let balance_json = await balance.json()
            assert.equal(balance_json.balance, 8)
        })
        it(`Send second VALID USE_FAUCET Transaction from ${test_wallet_pseudonym_6}%${wallets[6].address} for  ${test_wallet_pseudonym_5}%${wallets[5].address}'s token`, async function () {
            let transaction_data = {
                "token_issuer_id_pseudonym" : test_wallet_pseudonym_5,
                "token_issuer_id_public_key": wallets[5].address,
                "from_user_id_pseudonym"    : test_wallet_pseudonym_6,
                "signing_key"               : wallets[6].address,
                "to_user_id_pseudonym"      : test_wallet_pseudonym_6,
                "to_user_id_public_key"     : wallets[6].address,
                "transaction_code"          : "USE_FAUCET",
                "transaction_date"          : (new Date()).getTime(),
                "nonce"                     : 2,
                "previous_transaction_hash" : test_wallet_6_pervious_transaction_hash
            }
            let transaction_data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(transaction_data_string))
            test_wallet_6_pervious_transaction_hash = calculated_hash
            let signed_hash = await wallets[6].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data              : transaction_data_string,
                        hash              : calculated_hash,
                        signature_of_hash : signed_hash
                    }),
                    headers: {
                        access_token: test_wallet_6_access_token,
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code == "TRANSACTION_SUCCESSFUL", true)
            const balance = await fetch(
                testing_url + "/api/check_balance", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        token_issuer_id_pseudonym  : test_wallet_pseudonym_5,
                        token_issuer_id_public_key : wallets[5].address,
                        user_id_pseudonym          : test_wallet_pseudonym_6,
                        user_id_public_key         : wallets[6].address,
                        access_token: test_wallet_5_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let balance_json = await balance.json()
            assert.equal(balance_json.balance, 4)
        })
        it(`Send second VALID MEME Transaction of type STATEMENT from ${test_wallet_pseudonym_6}%${wallets[6].address} responding to a meme from ${test_wallet_pseudonym_5}%${wallets[5].address}`, async function () {
            let meme_data = {
                "created": (new Date()).getTime(),
                "text": {
                    title: "What I (Stacy) does for work.",
                    body : "I work at a nail salon part time while I go to school for Sociology. I once had a month where I had a different color for my nails every day, I used fakes half the time but still counts."
                },
                "type"           : "text",
                "root_meme"      : root_question_response,
                "meme_type_code" : "STATEMENT",
                "permissions"    : "REQUIRE_PAYMENT_RESPONDER"
            }
            let meme_data_string = JSON.stringify(meme_data)
            let meme_data_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(meme_data_string))
            // #TODO, was allowed to send my own token and get the meme through
            let transaction_data = {
                "token_issuer_id_pseudonym" : test_wallet_pseudonym_5,
                "token_issuer_id_public_key": wallets[5].address,
                "from_user_id_pseudonym"    : test_wallet_pseudonym_6,
                "signing_key"               : wallets[6].address,
                "to_user_id_pseudonym"      : "Zero",
                "to_user_id_public_key"     : "0x0000000000000000000000000000000000000000",
                "value"                     : 1,
                "transaction_code"          : "MEME",
                "transaction_date"          : (new Date()).getTime(),
                "nonce"                     : 3,
                "data_hash"                 : meme_data_hash,
                "previous_transaction_hash" : test_wallet_6_pervious_transaction_hash
            }
            let transaction_data_string = JSON.stringify(transaction_data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(transaction_data_string))
            test_wallet_6_pervious_transaction_hash = calculated_hash
            let signed_hash = await wallets[6].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/perform_transaction", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        internal_data     : meme_data_string,
                        data              : transaction_data_string,
                        hash              : calculated_hash,
                        signature_of_hash : signed_hash,
                        access_token: test_wallet_6_access_token
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code == "TRANSACTION_SUCCESSFUL", true)
            const balance = await fetch(
                testing_url + "/api/check_balance", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        token_issuer_id_pseudonym  : test_wallet_pseudonym_5,
                        token_issuer_id_public_key : wallets[5].address,
                        user_id_pseudonym          : test_wallet_pseudonym_6,
                        user_id_public_key         : wallets[6].address,
                        access_token: test_wallet_5_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let balance_json = await balance.json()
            assert.equal(balance_json.balance, 3)
        })
    })
})