import { ethers } from "ethers";
import fetch from 'node-fetch';
import assert from 'assert'
import { persona_names } from "./persona_names.js"
import Persona from "../helper_functions/Persona.js"
import { v4 as uuidv4 } from 'uuid';

let special_phase = "curve foster stay broccoli equal icon bamboo champion casino impact will damp"
let wallets = []
for (let i = 0; i < 14; i++) {
  wallets.push(ethers.Wallet.fromMnemonic(special_phase, `m/44'/60'/0'/0/${i}`))
}

let testing_url = "http://localhost:4000"


describe('Testing EDGE Transactions', async function () {
  let access_token = null
  let questions = null
  it('Login and get cookie', async function () {
    let data = {
      "pseudonym" : persona_names[12],
      "unix_time_ms" : (new Date()).getTime(),
      "signing_key" : wallets[12].address
    }
    let data_string = JSON.stringify(data)
    let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
    let signed_hash = await wallets[12].signMessage(calculated_hash)
    const response = await fetch(
        testing_url + "/api/wield_persona", 
        {
            method: 'POST', 
            body: JSON.stringify({
                data : data_string,
                hash : calculated_hash,
                signature_of_hash: signed_hash
            }),
            headers: {
                "content-type": "application/json"
            }
        }
    );
    let response_json = await response.json()
    access_token = response_json.data.ACCESS_TOKEN
  })
  it('Fetch the previous transactions', async function () {
    const response = await fetch(
      testing_url + "/api/query_memes", 
        {
            method: 'POST', 
            body: JSON.stringify({
                query_type : "PUBLIC_MEMES_FROM_PERSONA",
                query_data    : {
                    pseudonym  : persona_names[12],
                    public_key : wallets[12].address,
                },
                access_token: access_token,
            }),
            headers: {
                "content-type": "application/json"
            }
        }
    );
    let response_json = await response.json()
    // console.log("previous transactions")
    // console.log(response_json)
    questions = response_json.memes
  })
  let my_context = uuidv4()
  it('Add Edges Between the Previous Transactions', async function () {
    let tmp_persona = persona_names[12]
    let tmp_wallet  = wallets[12]
    tmp_persona = new Persona("EGREGORE", wallets[12].address, persona_names[12], testing_url)
    tmp_persona.setAccessToken(access_token)
    assert.equal(tmp_persona.access_token != null, true, "Problem with Access Token")
    for(var i = 0; i < questions.length - 1; i++){
      tmp_persona.setPreviousTransaction(  await tmp_persona.get_previous_transaction() )
      let edge_data = {
          "created": (new Date()).getTime(),
          "source_meme"    : questions[i].T_HASH_OF_MEME,
          "reference_meme" : questions[i+1].T_HASH_OF_MEME,
          "type" : "text",
          "edge_type_code": "NEXT_ITEM",
          "permissions": "PUBLIC",
          "context_id" : my_context,
          context_index : i
      }
      let transaction_data = await tmp_persona.GENERATE_EDGE_TRANSACTION(
          edge_data,
          persona_names[12],
          wallets[12].address,
          1
      )
      transaction_data.signature_of_hash = await tmp_wallet.signMessage( transaction_data.hash )
      let transaction_result = await tmp_persona.SEND_TRANSACTION(transaction_data)
      // console.log(transaction_data)
      // console.log(transaction_result)
      assert.equal("response_type" in transaction_result, true)
      assert.equal(transaction_result.response_type == "SUCCESS", true, "Got Error sending transaction")
    
    }
  })
})