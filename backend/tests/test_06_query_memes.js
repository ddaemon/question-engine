import { ethers } from "ethers";
import fetch from 'node-fetch';
import assert from 'assert'
import { persona_names } from "./persona_names.js"

let special_phase = "curve foster stay broccoli equal icon bamboo champion casino impact will damp"
let wallets = []
for (let i = 0; i < 10; i++) {
  wallets.push(ethers.Wallet.fromMnemonic(special_phase, `m/44'/60'/0'/0/${i}`))
}

let testing_url = "http://localhost:4000"

describe(`Let's query the meme graph`, async function () {
    let wallet_0_pseudonym = "Hayt"
    let test_wallet_pseudonym_0 = "Scytale" // Use wallet 5
    let test_wallet_0_access_token = null
    let test_wallet_0_pervious_transaction_hash = null
    let test_wallet_pseudonym_1 = "Otheym" // Use wallet 6
    let test_wallet_1_access_token = null
    let test_wallet_1_pervious_transaction_hash = null
    describe('/api/wield_persona for test wallets 5 and 6', async function () {
        it('make request with correct hash AND signature', async function () {
            let data = {
                "pseudonym" : test_wallet_pseudonym_0,
                "unix_time_ms" : (new Date()).getTime(),
                "signing_key" : wallets[5].address
            }
            let data_string = JSON.stringify(data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            let signed_hash = await wallets[5].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/wield_persona", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            test_wallet_0_access_token = response_json.data.ACCESS_TOKEN
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code, "NOW_LOGGED_IN")
        });
        it('make request with correct hash AND signature', async function () {
            let data = {
                "pseudonym" : test_wallet_pseudonym_1,
                "unix_time_ms" : (new Date()).getTime(),
                "signing_key" : wallets[6].address
            }
            let data_string = JSON.stringify(data)
            let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
            let signed_hash = await wallets[6].signMessage(calculated_hash)
            const response = await fetch(
                testing_url + "/api/wield_persona", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        data : data_string,
                        hash : calculated_hash,
                        signature_of_hash: signed_hash
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            test_wallet_1_access_token = response_json.data.ACCESS_TOKEN
            assert.equal("response_type" in response_json, true)
            assert.equal(response_json.response_type == "SUCCESS", true)
            assert.equal(response_json.response_code, "NOW_LOGGED_IN")
        });
    })
    describe('/api/query_memes for test wallets 5 and 6', async function () {
        it('Check if query_type produces correct error', async function () {
            const response = await fetch(
                testing_url + "/api/query_memes", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        query_type : "PUBLIC_MEMES_FROM_PERSONA",
                        query_data    : {
                            pseudonym  : persona_names[8],
                            public_key : wallets[8].address,
                        },
                        access_token: test_wallet_0_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal(response_json.memes.length == 2, true)
        })
    })
    describe('/api/query_memes for previous transactions', async function () {
        it('Check if query_type produces correct error', async function () {
            const response = await fetch(
                testing_url + "/api/query_memes", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        query_type : "TRANSACTIONS_FROM",
                        query_data    : {
                            pseudonym  : wallet_0_pseudonym,
                            public_key : wallets[0].address
                        },
                        access_token: test_wallet_0_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            assert.equal(response_json.transactions.length == 3, true)
        })
    })
    describe('/api/query_memes for REVERSE_LOOKUP_PSEUDONYM', async function () {
        it('Check if query_type produces correct error', async function () {
            const response = await fetch(
                testing_url + "/api/query_memes", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        query_type : "REVERSE_LOOKUP_PSEUDONYM",
                        query_data : {
                            public_key : wallets[0].address
                        },
                        access_token: test_wallet_0_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            // assert.equal(response_json.transactions.length == 3, true)
        })
        it('Check if query_type produces correct error', async function () {
            const response = await fetch(
                testing_url + "/api/query_memes", 
                {
                    method: 'POST', 
                    body: JSON.stringify({
                        query_type : "MY_LAST_TRANSACTION",
                        query_data : {
                            public_key : wallets[0].address
                        },
                        access_token: test_wallet_1_access_token,
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                }
            );
            let response_json = await response.json()
            // assert.equal(response_json.transactions.length == 3, true)
        })
    })
})