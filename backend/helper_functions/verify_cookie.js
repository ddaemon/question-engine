export async function verify_cookie(ACCESS_TOKEN, db){
    let query_bearer_token = await db.models.AUTH_T.findAll({
        where: {
            ACCESS_TOKEN: ACCESS_TOKEN
        }
    })
    if (query_bearer_token.length == 0){
        return {
            "response_type"  : "ERROR",
            "response_code"  : "CAN_NOT_FIND_COOKIE",
            "description"    : `Please request a new one at /get_cookie`
        }
    }
    if ('DATE_COOKIE_EXPIRED' in query_bearer_token[0]){
        if(query_bearer_token[0].DATE_COOKIE_EXPIRED > new Date()){
            if('USER_ID' in query_bearer_token[0]){
                return {
                    "response_type"  : "SUCCESS",
                    "response_code"  : "LOGGED_IN",
                    "description"    : "User is not logged in",
                    USER_ID          : query_bearer_token[0].USER_ID,
                    PSEUDONYM        : query_bearer_token[0].PSEUDONYM,
                    PUBLIC_KEY       : query_bearer_token[0].PUBLIC_KEY
                }
            }
            else {
                return {
                    "response_type" : "SUCCESS",
                    "response_code" : "NOT_LOGGED_IN",
                    "description"   : `Here is the cookie we already gave you`,
                    USER_ID         : query_bearer_token[0].USER_ID,
                    PSEUDONYM       : query_bearer_token[0].PSEUDONYM,
                    PUBLIC_KEY      : query_bearer_token[0].PUBLIC_KEY,
                    DATE_EXPIRED    : query_bearer_token[0].DATE_EXPIRED
                }
            }
        }
        else {
            return {
                "response_type" : "ERROR",
                "response_code" : "COOKIE_EXPIRED",
                "description"   : `You cookie expired${query_bearer_token[0].DATE_COOKIE_EXPIRED} please request a new one at /get_cookie`
            }
        }
    } else {
        return {
            "response_type"  : "ERROR",
            "response_code"  : "NO_COOKIE_EXISTS",
            "description"    : `Unknown cookie error`
        }
    }
} 
