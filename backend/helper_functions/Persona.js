import { ethers } from "ethers";
import fetch from 'node-fetch';

export default class Persona {
  constructor( persona_type, evm_address, pseudonym = "", testing_url = "") {
    let persona_types = ["EGREGORE", "WEARING"]
    if ( persona_type.includes(persona_types) ){
      return ({
        "response_type"  : "ERROR",
        "response_code"  : "INVALID_PERSONA_CONSTRUCTOR",
        "description"    : "PERSONA TYPE MUST BE EGREGORE or WEARING"
      })
    }
    this.persona_type = persona_type
    this.setEVMAddress( ethers.utils.getAddress(evm_address) )
    this.access_token = null
    this.transaction_count = null
    this.previous_transaction_hash = null
    this.tokens_owned = null
    this.token_balances = null
    this.previous_transactions = null
    this.memes = null
    this.setPseudonym(pseudonym)
    this.testing_url = testing_url
    this.valid_persona = false
    this.nonce = null
    this.previous_transaction_hash = null
    this.signed_in = false
    this.previous_transaction = null
    this.sign_in_json = null
  }

  setEVMAddress(evm_address){
    this.evm_address = evm_address
  }


  getEVMAddress(){
    return this.evm_address
  }
  setSigner(signer){
    // TODO
    this.signer = signer
  }
  setAccessToken(tmp_access_token){
    this.access_token = tmp_access_token
  }

  setPseudonym(pseudonym) {
    this.pseudonym = pseudonym;
  }
  setValidity( validity ) {
    this.valid_persona = validity;
  }
  setSignedIn(signed_in_state){
    if (signed_in_state == false){
      return false
    }
    else {
      this.signed_in = true
      this.access_token  = signed_in_state.data.ACCESS_TOKEN
      this.persona_type = "WEARING"
      return true
    }
  }
  setPreviousTransaction(transaction_data){
    if(transaction_data != false){
      this.nonce = transaction_data[0].TRANSACTION_NONCE
      this.previous_transaction_hash = transaction_data[0].TRANSACTION_DATA_HASH
      this.previous_transaction = transaction_data
    }
  }
  setPreviousTransactionHash(previous_transaction_hash){
    this.previous_transaction_hash = previous_transaction_hash
  }
  setNonce(nonce){
    this.nonce = nonce
  }



  async lookupPseudonym(tmp_evm_address){
    let pseudonym_query =  await fetch(
      this.testing_url + "/api/query_memes", 
      {
          method: 'POST', 
          body: JSON.stringify({
              query_type : "REVERSE_LOOKUP_PSEUDONYM",
              query_data : {
                  public_key : tmp_evm_address
              }
          }),
          headers: {
              Cookie: this.access_token,
              "content-type": "application/json"
          }
      }
    )
    let pseudonym_result = await pseudonym_query.json()
    if (pseudonym_result.response_type == "ERROR"){
      return false
    }
    return pseudonym_result.query_result.Persona.pseudonym
  }

  async check_persona_exists(tmp_evm_address, tmp_pseudonym){
    let query =  await fetch(
      this.testing_url + "/api/query_memes", 
      {
          method: 'POST', 
          body: JSON.stringify({
              query_type : "CHECK_VALID_PERSONA",
              query_data : {
                  public_key : tmp_evm_address,
                  pseudonym  : tmp_pseudonym
              }
          }),
          headers: {
              Cookie: this.access_token,
              "content-type": "application/json"
          }
      }
    )
    let result = await query.json()
    if (result.response_type == "ERROR"){
      return false
    }
    return true
  }

  async generate_sign_in_hash(){
    if(this.validity == false){
      return false
    }
    let data = {
        "pseudonym"    : this.pseudonym,
        "unix_time_ms" : (new Date()).getTime(),
        "signing_key"  : this.evm_address
    }
    let data_string = JSON.stringify(data)
    let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
    return {
        data : data_string,
        hash : calculated_hash,
        signature_of_hash: "TODO"
    }
  }

  async sign_in(sign_in_json){
    const response = await fetch(
      this.testing_url + "/api/wield_persona", 
      {
          method: 'POST', 
          body: JSON.stringify(sign_in_json),
          headers: {
              "content-type": "application/json"
          }
      }
    );
    let response_json = await response.json()
    if (response_json.response_type == "ERROR"){
      return false
    }
    else {
      return response_json
    }
  }
  async get_transaction_count(){
    if(this.validity == false){
      return false
    }
    let query =  await fetch(
      this.testing_url + "/api/query_memes", 
      {
          method: 'POST', 
          body: JSON.stringify({
              query_type : "GET_TRANSACTION_COUNT",
              query_data : {
                pseudonym  : this.pseudonym,
                public_key : this.evm_address
              }
          }),
          headers: {
              Cookie: this.access_token,
              "content-type": "application/json"
          }
      }
    )
    let result = await query.json()
    if (result.response_type == "ERROR"){
      return false
    }
    return result.query_result
  }

  async get_previous_transaction(){
    if(this.validity == false){
      return false
    }
    let query =  await fetch(
      this.testing_url + "/api/query_memes", 
      {
          method: 'POST', 
          body: JSON.stringify({
              query_type : "PERSONA_LAST_TRANSACTION",
              query_data : {
                pseudonym  : this.pseudonym,
                public_key : this.evm_address
              },
              access_token : this.access_token
          }),
          headers: {
              "content-type": "application/json"
          }
      }
    )
    let result = await query.json()
    if (result.response_type == "ERROR"){
      return false
    }
    return result.query_result
  }

  async GENERATE_INITIAL_MINT_TRANSACTION(tmp_value){
    let transaction_data = {
      "token_issuer_id_pseudonym" : this.pseudonym,
      "token_issuer_id_public_key": this.evm_address,
      "from_user_id_pseudonym"    : this.pseudonym,
      "signing_key"               : this.evm_address,
      "to_user_id_pseudonym"      : this.pseudonym,
      "to_user_id_public_key"     : this.evm_address,
      "value"                     : tmp_value,
      "transaction_code"          : "MINT",
      "transaction_date"          : (new Date()).getTime(),
      "nonce"                     : 1
    }
    let data_string = JSON.stringify(transaction_data)
    let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
    return {
      data : data_string,
      hash : calculated_hash,
      signature_of_hash: false,
      access_token: this.access_token,
    }
  }

  async GENERATE_MINT_TRANSACTION(tmp_value){
    let transaction_data = {
      "token_issuer_id_pseudonym" : this.pseudonym,
      "token_issuer_id_public_key": this.getEVMAddress(),
      "from_user_id_pseudonym"    : this.pseudonym,
      "signing_key"               : this.getEVMAddress(),
      "to_user_id_pseudonym"      : this.pseudonym,
      "to_user_id_public_key"     : this.getEVMAddress(),
      "value"                     : tmp_value,
      "transaction_code"          : "MINT",
      "transaction_date"          : (new Date()).getTime(),
      "nonce"                     : this.nonce + 1,
      "previous_transaction_hash" : this.previous_transaction_hash
    }
    let data_string = JSON.stringify(transaction_data)
    let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
    return {
      data : data_string,
      hash : calculated_hash,
      signature_of_hash: false,
      access_token: this.access_token,
    }
  }

  async GENERATE_BURN_TRANSACTION(issuer_pseudoum, issuer_address, tmp_value){
    let transaction_data = {
      "token_issuer_id_pseudonym" : issuer_pseudoum,
      "token_issuer_id_public_key": issuer_address,
      "from_user_id_pseudonym"    : this.pseudonym,
      "signing_key"               : this.evm_address,
      "to_user_id_pseudonym"      : "Zero",
      "to_user_id_public_key"     : '0x0000000000000000000000000000000000000000',
      "value"                     : tmp_value,
      "transaction_code"          : "BURN",
      "transaction_date"          : (new Date()).getTime(),
      "nonce"                     : this.nonce + 1,
      "previous_transaction_hash" : this.previous_transaction_hash
    }
    let data_string = JSON.stringify(transaction_data)
    let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
    return {
      data : data_string,
      hash : calculated_hash,
      signature_of_hash: false,
      access_token: this.access_token,
    }
  }

  async GENERATE_REMOVE_TRANSACTION(remove_from_pseudonym, remove_from_evm_address, tmp_value){
    let transaction_data = {
      "token_issuer_id_pseudonym" : this.pseudonym,
      "token_issuer_id_public_key": this.evm_address,
      "from_user_id_pseudonym"    : this.pseudonym,
      "signing_key"               : this.evm_address,
      "to_user_id_pseudonym"      : remove_from_pseudonym, // This is who you remove from
      "to_user_id_public_key"     : remove_from_evm_address,
      "value"                     : tmp_value,
      "transaction_code"          : "REMOVE",
      "transaction_date"          : (new Date()).getTime(),
      "nonce"                     : this.nonce + 1,
      "previous_transaction_hash" : this.previous_transaction_hash
    }
    let data_string = JSON.stringify(transaction_data)
    let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
    return {
      data : data_string,
      hash : calculated_hash,
      signature_of_hash: false,
      access_token: this.access_token,
    }
  }

  async GENERATE_TRANSFER_TRANSACTION(issuer_pseudoum, issuer_evm_address, to_pseudonym, to_evm_address, tmp_value){
    let transaction_data = {
      "token_issuer_id_pseudonym" : issuer_pseudoum,
      "token_issuer_id_public_key": issuer_evm_address,
      "from_user_id_pseudonym"    : this.pseudonym,
      "signing_key"               : this.evm_address,
      "to_user_id_pseudonym"      : to_pseudonym,
      "to_user_id_public_key"     : to_evm_address,
      "value"                     : tmp_value,
      "transaction_code"          : "TRANSFER",
      "transaction_date"          : (new Date()).getTime(),
      "nonce"                     : this.nonce + 1,
      "previous_transaction_hash" : this.previous_transaction_hash
    }
    let data_string = JSON.stringify(transaction_data)
    let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
    return {
      data : data_string,
      hash : calculated_hash,
      signature_of_hash: false,
      access_token: this.access_token,
    }
  }

  async GENERATE_MEME_TRANSACTION( meme_data, token_issuer_pseudonym, token_issuer_address, value ){
    let meme_data_string = JSON.stringify(meme_data)
    let meme_data_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(meme_data_string))
    let transaction_data = {
      "token_issuer_id_pseudonym" : token_issuer_pseudonym,
      "token_issuer_id_public_key": token_issuer_address,
      "from_user_id_pseudonym"    : this.pseudonym,
      "signing_key"               : this.evm_address,
      "to_user_id_pseudonym"      : "Zero",
      "to_user_id_public_key"     : "0x0000000000000000000000000000000000000000",
      "value"                     : value,
      "transaction_code"          : "MEME",
      "transaction_date"          : (new Date()).getTime(),
      "nonce"                     : this.nonce + 1,
      "previous_transaction_hash" : this.previous_transaction_hash,
      "data_hash"                 : meme_data_hash,
    }
    let transaction_data_string = JSON.stringify(transaction_data)
    let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(transaction_data_string))
    return {
      internal_data     : meme_data_string,
      data              : transaction_data_string,
      hash              : calculated_hash,
      signature_of_hash : false,
      access_token      : this.access_token
    }
  }


  async GENERATE_GROUP_CREATE_TRANSACTION( group_data, token_issuer_pseudonym, token_issuer_address, to_pseudonym, to_evm_address, value ){
    let group_data_string = JSON.stringify(group_data)
    let group_data_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(group_data_string))
    let transaction_data = {
      "token_issuer_id_pseudonym" : token_issuer_pseudonym,
      "token_issuer_id_public_key": token_issuer_address,
      "from_user_id_pseudonym"    : this.pseudonym,
      "signing_key"               : this.evm_address,
      "to_user_id_pseudonym"      : to_pseudonym,
      "to_user_id_public_key"     : to_evm_address,
      "value"                     : value,
      "transaction_code"          : "GROUP_CREATE",
      "transaction_date"          : (new Date()).getTime(),
      "nonce"                     : this.nonce + 1,
      "previous_transaction_hash" : this.previous_transaction_hash,
      "data_hash"                 : group_data_hash,
    }
    let transaction_data_string = JSON.stringify(transaction_data)
    let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(transaction_data_string))
    return {
      internal_data     : group_data_string,
      data              : transaction_data_string,
      hash              : calculated_hash,
      signature_of_hash : false,
      access_token      : this.access_token
    }
  }

  async GENERATE_GROUP_MANAGE_TRANSACTION( group_data, token_issuer_pseudonym, token_issuer_address, to_pseudonym, to_evm_address, value ){
    let group_data_string = JSON.stringify(group_data)
    let group_data_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(group_data_string))
    let transaction_data = {
      "token_issuer_id_pseudonym" : token_issuer_pseudonym,
      "token_issuer_id_public_key": token_issuer_address,
      "from_user_id_pseudonym"    : this.pseudonym,
      "signing_key"               : this.evm_address,
      "to_user_id_pseudonym"      : to_pseudonym,
      "to_user_id_public_key"     : to_evm_address,
      "value"                     : value,
      "transaction_code"          : "GROUP_MANAGE",
      "transaction_date"          : (new Date()).getTime(),
      "nonce"                     : this.nonce + 1,
      "previous_transaction_hash" : this.previous_transaction_hash,
      "data_hash"                 : group_data_hash,
    }
    let transaction_data_string = JSON.stringify(transaction_data)
    let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(transaction_data_string))
    return {
      internal_data     : group_data_string,
      data              : transaction_data_string,
      hash              : calculated_hash,
      signature_of_hash : false,
      access_token      : this.access_token
    }
  }

  async GENERATE_EDGE_TRANSACTION( tx_data, token_issuer_pseudonym, token_issuer_address, value ){
    let tx_data_string = JSON.stringify(tx_data)
    let tx_data_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(tx_data_string))
    let transaction_data = {
      "token_issuer_id_pseudonym" : token_issuer_pseudonym,
      "token_issuer_id_public_key": token_issuer_address,
      "from_user_id_pseudonym"    : this.pseudonym,
      "signing_key"               : this.evm_address,
      "to_user_id_pseudonym"      : "Zero",
      "to_user_id_public_key"     : "0x0000000000000000000000000000000000000000",
      "value"                     : value,
      "transaction_code"          : "EDGE",
      "transaction_date"          : (new Date()).getTime(),
      "nonce"                     : this.nonce + 1,
      "previous_transaction_hash" : this.previous_transaction_hash,
      "data_hash"                 : tx_data_hash,
    }
    let transaction_data_string = JSON.stringify(transaction_data)
    let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(transaction_data_string))
    return {
      internal_data     : tx_data_string,
      data              : transaction_data_string,
      hash              : calculated_hash,
      signature_of_hash : false,
      access_token      : this.access_token
    }
  }

  async SEND_TRANSACTION( transaction_data ){
    const response = await fetch(
      this.testing_url + "/api/perform_transaction", 
      {
          method: 'POST', 
          body: JSON.stringify( transaction_data ),
          headers: {
              "content-type": "application/json"
          }
      }
    )
    let response_json = await response.json()
    return response_json
  }

  async get_my_token_balance( issuer_pseudonym, issuer_evm_address ){
    // #TODO Check tokens object
    const balance = await fetch(
      this.testing_url + "/api/query_memes", 
      {
          method: 'POST', 
          body: JSON.stringify({
              query_type                 : "BALANCE",
              query_data                 : {
                token_issuer : {
                  public_key : issuer_evm_address,
                  pseudonym  : issuer_pseudonym 
                },
                token_owner : {
                  public_key : this.evm_address,
                  pseudonym  : this.pseudonym 
                }
                
              },
              access_token: this.access_token
          }),
          headers: {
              "content-type": "application/json"
          }
      }
    );
    let balance_json = await balance.json()
    return balance_json
  }
  get_previous_transactions( TODO ){
    return true
  }

}