import { ethers } from "ethers"
export async function verify_hash_and_signature(request_body){
    let keys_needed = ['data', 'hash', 'signature_of_hash']
    if (  keys_needed.every(key => Object.keys(request_body).includes(key))  ) {
        let calculated_hash = await ethers.utils.keccak256(ethers.utils.toUtf8Bytes(request_body.data))
        if (calculated_hash == request_body.hash) {
            let data_json = null
            try {
                data_json = JSON.parse(request_body.data)
            }
            catch {
                return({
                    "response_type" : "ERROR",
                    "response_code" : "DATA_JSON_NOT_PARSABLE",
                    "description"   : "field data can't be parsed by JSON.parse"
                })
            }
            if ("signing_key" in data_json) { 
                let verifySignature = ethers.utils.verifyMessage(calculated_hash, request_body.signature_of_hash)
                if (ethers.utils.getAddress(verifySignature) == ethers.utils.getAddress(data_json.signing_key)){
                    return(true)
                }
                else {
                    return({
                        "response_type"  : "ERROR",
                        "response_code"  : "INVALID_SIGNATURE_ON_DATA_HASH",
                        "description"    : `We got the following signature ${verifySignature} and you signed ${data_json.signing_key}`
                    })
                }
            } else {
                return({
                    "response_type" : "ERROR",
                    "response_code" : "NO_SIGNING_KEY",
                    "description"   : "No 'signing_key' key in data JSON string"
                })
            }
        } else {
            return({
                "response_type" : "ERROR",
                "response_code" : "INCORRECT_HASH_OF_DATA",
                "description"   : "Hash does not match"
            })
        }
    } else {
        return({
            "response_type" : "ERROR",
            "response_code" : "MISSING_KEYS",
            "description"   : "Missing keys, keys_needed = ['data', 'hash', 'signature_of_hash']"
        })
    }
}
