import { verify_cookie } from "./verify_cookie.js"

export async function query_memes(request_body, db){
    // if (  !("query_type" in request_body)  ){
    //     return ({
    //         "response_type"  : "ERROR",
    //         "response_code"  : "INVALID_QUERY_JSON",
    //         "description"    : "Forgot query_type"
    //     })
    // }
    if( request_body.query_type == "REVERSE_LOOKUP_PSEUDONYM" ){
        if( !("public_key" in request_body.query_data) ){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "query_type persona requires pseudonym and public_key keys"
            })
        }
        let query_persona = await db.models.USER_T.findOne({
            where: {
                PUBLIC_KEY  : request_body.query_data.public_key
            }
        })
        if (query_persona == null){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "Can't find that Persona with that pseudonym and public key"
            })
        }
        return {
            "response_type"  : "SUCCESS",
            "response_code"  : "MEME_DELIVERY",
            "description"    : "Result for REVERSE_LOOKUP_PSEUDONYM",
            "query_result"   : {
                Persona: {
                    pseudonym  : query_persona.PSEUDONYM,
                    public_key : request_body.query_data.public_key
                }
            }
        }
    }

    if( request_body.query_type == "CHECK_VALID_PERSONA" ){
        if( !("public_key" in request_body.query_data) && !("pseudonym" in request_body.query_data)){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "query_type persona requires pseudonym and public_key keys"
            })
        }
        let query_persona = await db.models.USER_T.findOne({
            where: {
                PUBLIC_KEY  : request_body.query_data.public_key,
                PSEUDONYM   : request_body.query_data.pseudonym
            }
        })
        if (query_persona == null){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "Can't find that Persona with that pseudonym and public key"
            })
        }
        return {
            "response_type"  : "SUCCESS",
            "response_code"  : "MEME_DELIVERY",
            "description"    : "Result for REVERSE_LOOKUP_PSEUDONYM",
            "query_result"   : {
                Persona: {
                    pseudonym  : query_persona.PSEUDONYM,
                    public_key : request_body.query_data.public_key
                }
            }
        }
    }


    // Querys beyond here require verification
    if ( !("access_token" in request_body) ) {
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "REQUIRES_ACCESS_TOKEN",
            "description"    : "Can't find that Persona with that pseudonym and public key"
        })
    }
    let cookie_status = false
    if (request_body.access_token != undefined) {
        cookie_status = await verify_cookie(request_body.access_token, db)
    }
    if(cookie_status.response_type == "ERROR" || cookie_status.response_code == "NOT_LOGGED_IN"){
        return(cookie_status)
    }
    let user_uuid = cookie_status.USER_ID


    if( request_body.query_type == "GET_TRANSACTION_COUNT" ){
        let query_persona = await db.models.USER_T.findOne({
            where: {
                PUBLIC_KEY  : request_body.query_data.public_key,
                PSEUDONYM   : request_body.query_data.pseudonym
            }
        })
        if (query_persona == null){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "Can't find that Persona with that pseudonym and public key"
            })
        }
        console.log(query_persona)
        let result = await db.models.TRANSACTIONS_T.count({
            where: {
                FROM_USER_ID  : query_persona.USER_ID
            }
        })
        if (result == null){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "Can't find that Persona with that pseudonym and public key"
            })
        }
        return {
            "response_type"  : "SUCCESS",
            "response_code"  : "MEME_DELIVERY",
            "description"    : "Result for GET_TRANSACTION_COUNT",
            "query_result"   : result
        }
    }


    // Specific Query Code Below
    if( request_body.query_type == "PUBLIC_MEMES_REVERSE_CHRONOLOGICAL" ){
        let query_memes = await db.models.MEMES_T.findAll({
            where : {
                MEME_PERMISSIONS : "PUBLIC"
            },
            order : [['updatedAt', 'DESC']]
        })
        if (query_memes == null){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "This user does not have any public meme's, you can make a meme for them if you like"
            })
        }
        return {
            "response_type"  : "SUCCESS",
            "response_code"  : "MEME_DELIVERY",
            "description"    : "Here are the public memes in reverse chronological order",
            "memes" : query_memes
        }
    }

    if( request_body.query_type == "PUBLIC_MEMES_CHRONOLOGICAL" ){
        let query_memes = await db.models.MEMES_T.findAll({
            where : {
                MEME_PERMISSIONS : "PUBLIC"
            },
            order : [['updatedAt', 'ASC']]
        })
        if (query_memes == null){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "This user does not have any public meme's, you can make a meme for them if you like"
            })
        }
        return {
            "response_type"  : "SUCCESS",
            "response_code"  : "MEME_DELIVERY",
            "description"    : "#TODO",
            "memes" : query_memes
        }
    }

    if( request_body.query_type == "PUBLIC_MEMES_FROM_PERSONA" ){
        if( !("pseudonym" in request_body.query_data)  || !("public_key" in request_body.query_data) ){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "query_type persona requires pseudonym and public_key keys"
            })
        }
        let query_persona = await db.models.USER_T.findOne({
            where: {
                PUBLIC_KEY  : request_body.query_data.public_key,
                PSEUDONYM   : request_body.query_data.pseudonym
            }
        })
        if (query_persona == null){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "Can't find that Persona with that pseudonym and public key"
            })
        }
        // Validation Code Ends
        let query_memes = await db.models.MEMES_T.findAll({
            // include: { model: db.models.TRANSACTIONS_T, required: true },
            where : {
                AUTHOR_USER_ID   : query_persona.USER_ID,
                MEME_PERMISSIONS : "PUBLIC"
            },
            order : [
                ["createdAt", "asc"]
            ]

        })
        if (query_memes == null){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "This user does not have any public meme's, you can make a meme for them if you like"
            })
        }
        return {
            "response_type"  : "SUCCESS",
            "response_code"  : "MEME_DELIVERY",
            "description"    : "#TODO",
            "memes" : query_memes
        }
    }


    if( request_body.query_type == "PERSONA_LAST_TRANSACTION" ){
        if( !("pseudonym" in request_body.query_data)  || !("public_key" in request_body.query_data) ){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "query_type persona requires pseudonym and public_key keys"
            })
        }
        let query_persona = await db.models.USER_T.findOne({
            where: {
                PUBLIC_KEY  : request_body.query_data.public_key,
                PSEUDONYM   : request_body.query_data.pseudonym
            }
        })
        if (query_persona == null){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "Can't find that Persona with that pseudonym and public key"
            })
        }
        let query_transaction = await db.models.TRANSACTIONS_T.findOne({
            where: {
                FROM_USER_ID     : query_persona.USER_ID
            },
            order : [
                ["TRANSACTION_NONCE", "DESC"]
            ]
        })
        return {
            "response_type"  : "SUCCESS",
            "response_code"  : "MEME_DELIVERY",
            "description"    : "Result for REVERSE_LOOKUP_PSEUDONYM",
            "query_result"   : [ query_transaction ]
        }
    }

    if( request_body.query_type == "PUBLIC_RESPONSES_TO_MEME" ){
        if( !("pseudonym" in request_body.persona)  || !("public_key" in request_body.persona) ){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "query_type persona requires pseudonym and public_key keys"
            })
        }
        let query_persona = await db.models.USER_T.findOne({
            where: {
                PUBLIC_KEY  : request_body.persona.public_key,
                PSEUDONYM   : request_body.persona.pseudonym
            }
        })
        if (query_persona == null){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "Can't find that Persona with that pseudonym and public key"
            })
        }
        // Validation Code Ends
        let query_memes = await db.models.MEMES_T.findAll({
            where : {
                MEME_PERMISSIONS : "PUBLIC",
                
            }

        })
        if (query_memes == null){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "This user does not have any public meme's, you can make a meme for them if you like"
            })
        }
        return {
            "response_type"  : "SUCCESS",
            "response_code"  : "MEME_DELIVERY",
            "description"    : "#TODO",
            "memes" : query_memes
        }
    }

    if( request_body.query_type == "MY_REVEALED_MEMES" ){
        if( !("pseudonym" in request_body.persona)  || !("public_key" in request_body.persona) ){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "query_type persona requires pseudonym and public_key keys"
            })
        }
        let query_persona = await db.models.USER_T.findOne({
            where: {
                PUBLIC_KEY  : request_body.persona.public_key,
                PSEUDONYM   : request_body.persona.pseudonym
            }
        })
        if (query_persona == null){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "Can't find that Persona with that pseudonym and public key"
            })
        }
        // Validation Code Ends
        return {
            "response_type"  : "ERROR",
            "response_code"  : "TODO",
            "description"    : "#TODO"
        }
    }

    if( request_body.query_type == "MEME_THREAD" ){
        if( !("pseudonym" in request_body.persona)  || !("public_key" in request_body.persona) ){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "query_type persona requires pseudonym and public_key keys"
            })
        }
        let query_persona = await db.models.USER_T.findOne({
            where: {
                PUBLIC_KEY  : request_body.persona.public_key,
                PSEUDONYM   : request_body.persona.pseudonym
            }
        })
        if (query_persona == null){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "Can't find that Persona with that pseudonym and public key"
            })
        }
        // Validation Code Ends
        return {
            "response_type"  : "ERROR",
            "response_code"  : "TODO",
            "description"    : "#TODO"
        }
    }

    if( request_body.query_type == "BALANCE" ){
        if( !("token_issuer" in request_body.query_data)  || !("token_owner" in request_body.query_data) ){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "query_type persona requires pseudonym and public_key keys"
            })
        }
        if( !("public_key" in request_body.query_data.token_issuer)  || !("pseudonym" in request_body.query_data.token_issuer) ){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "query_type persona requires pseudonym and public_key keys"
            })
        }
        if( !("public_key" in request_body.query_data.token_owner)  || !("pseudonym" in request_body.query_data.token_owner) ){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "query_type persona requires pseudonym and public_key keys"
            })
        }
        let issuer_persona = await db.models.USER_T.findOne({
            where: {
                PUBLIC_KEY  : request_body.query_data.token_issuer.public_key,
                PSEUDONYM   : request_body.query_data.token_issuer.pseudonym
            }
        })
        if (issuer_persona == null){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "Invalid Issuer"
            })
        }
        let owner_persona = await db.models.USER_T.findOne({
            where: {
                PUBLIC_KEY  : request_body.query_data.token_owner.public_key,
                PSEUDONYM   : request_body.query_data.token_owner.pseudonym
            }
        })
        if (owner_persona == null){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "Invalid Owner"
            })
        }
        let balance = await db.models.BALANCES_T.findOne({
            where: {
                USER_TOKEN  : issuer_persona.USER_ID,
                TOKEN_OWNER : owner_persona.USER_ID
            }
        })
        if (balance == null){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "There is no Balance"
            })
        }
        // Validation Code Ends
        return {
            "response_type"  : "SUCCESS",
            "response_code"  : "TODO",
            "description"    : "#TODO",
            "data"           : balance
        }
    }

    if( request_body.query_type == "TRANSACTIONS_FROM" ){
        if( !("query_data" in request_body) ){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "query_type persona requires pseudonym and TRANSACTIONS_FROM keys"
            })
        }
        if( !("pseudonym" in request_body.query_data)  || !("public_key" in request_body.query_data) ){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "query_type persona requires pseudonym and public_key keys"
            })
        }
        let query_persona = await db.models.USER_T.findOne({
            where: {
                PUBLIC_KEY  : request_body.query_data.public_key,
                PSEUDONYM   : request_body.query_data.pseudonym
            }
        })
        if (query_persona == null){
            return ({
                "response_type"  : "ERROR",
                "response_code"  : "INVALID_QUERY_JSON",
                "description"    : "Can't find that Persona with that pseudonym and public key"
            })
        }
        // Validation Code Ends
        let transactions = await db.models.TRANSACTIONS_T.findAll({
            where: {
                FROM_USER_ID_PUBLIC_KEY  : request_body.query_data.public_key,
                FROM_USER_ID_PSEUDONYM   : request_body.query_data.pseudonym
            },
            order : [
                ["FROM_USER_ID_PSEUDONYM", "DESC"]
            ]

        })
        return {
            "response_type"  : "SUCCESS",
            "response_code"  : "TRANSACTIONS_RETURNED",
            "description"    : "#TODO add the types of queries here",
            "transactions"   : transactions
        }
        
    }

    return {
        "response_type"  : "ERROR",
        "response_code"  : "INVALID_QUERY_TYPE",
        "description"    : "#TODO add the types of queries here"
    }
}