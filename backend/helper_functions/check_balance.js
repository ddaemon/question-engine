export async function check_balance(req_body, db){
    if(Object.keys(req_body).length != 5){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Not correct number of keys"
        })
    }
    let keys_needed = [
        'token_issuer_id_pseudonym',
        'token_issuer_id_public_key', 
        'user_id_pseudonym',
        'user_id_public_key'
    ]
    if (  !keys_needed.every(key => Object.keys(req_body).includes(key))  ) {
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Does not have correct keys in TRANSACTION_DATA"
        })
    }

    let get_token_uuid = await db.models.USER_T.findOne({
        where: {
            PSEUDONYM   : req_body.token_issuer_id_pseudonym,
            PUBLIC_KEY  : req_body.token_issuer_id_public_key
        }
    })
    if (get_token_uuid == null){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Can't find that token with that public key and pseudonym"
        })
    }
    let get_user_uuid = await db.models.USER_T.findOne({
        where: {
            PSEUDONYM   : req_body.user_id_pseudonym,
            PUBLIC_KEY  : req_body.user_id_public_key
        }
    })
    if (get_user_uuid == null){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "Can't find that user with that public key and pseudonym"
        })
    }
    let check_token_balance = await db.models.BALANCES_T.findOne({
        where: {
            USER_TOKEN  : get_token_uuid.dataValues.USER_ID,
            TOKEN_OWNER : get_user_uuid.dataValues.USER_ID
        }
    })
    if (check_token_balance == null){
        return ({
            "response_type"  : "ERROR",
            "response_code"  : "INVALID_DATA_JSON",
            "description"    : "No balance exists"
        })
    }
    req_body.balance = check_token_balance.dataValues.TOKEN_AMOUNT
    return req_body
}