export async function verify_valid_transaction(transaction_data, transaction_user_id, db){
    let keys_needed = ["nonce", "transaction_date"]
    if ( !keys_needed.every(key => Object.keys(transaction_data).includes(key)) ){
        return {
            "response_type" : "ERROR",
            "response_code" : "INVALID_TRANSACTION",
            "description"   : 'Missing keys in signed data please use ["nonce", "transaction_date"]'
        }
    }
    let users_previous_transaction = await db.models.TRANSACTIONS_T.findOne({
        where : {
            FROM_USER_ID : transaction_user_id
        },
        order : [
            ['TRANSACTION_NONCE', 'DESC']
        ]
    })
    if (users_previous_transaction === null && transaction_data.nonce == 1){
        return true
    }
    if (users_previous_transaction == 0 && transaction_data.nonce != 1){
        return {
            "response_type" : "ERROR",
            "response_code" : "INVALID_TRANSACTION",
            "description"   : "No nonce on chain (database), please set it to one"
        }
    } 
    if( transaction_data["previous_transaction_hash"] === undefined ){
        return {
            "response_type" : "ERROR",
            "response_code" : "INVALID_TRANSACTION",
            "description"   : "Forgot previous_transaction_hash"
        }
    }
    if ( parseInt(users_previous_transaction.dataValues.TRANSACTION_NONCE) + 1 != parseInt(transaction_data.nonce) ){
        return {
            "response_type" : "ERROR",
            "response_code" : "INVALID_TRANSACTION",
            "description"   : `Nonce invalid, last nonce was ${users_previous_transaction.dataValues.TRANSACTION_NONCE} you signed nonce = ${transaction_data.nonce} it should be ${users_previous_transaction.dataValues.TRANSACTION_NONCE + 1}`
        }
    }
    if (users_previous_transaction.dataValues.TRANSACTION_DATA_HASH != transaction_data.previous_transaction_hash){
        return {
            "response_type" : "ERROR",
            "response_code" : "INVALID_TRANSACTION",
            "description"   : `You signed the wrong transaction hash please use ${users_previous_transaction.dataValues.TRANSACTION_DATA_HASH}`
        }     
    }
    if (users_previous_transaction.dataValues.TRANSACTION_DATE > new Date(transaction_data.transaction_date) ){
        return {
            "response_type" : "ERROR",
            "response_code" : "INVALID_TRANSACTION",
            "description"   : `You trying to do a transaction before your last one, nope make sure signed after ${users_previous_transaction.dataValues.TRANSACTION_DATE}`
        }     
    }
    return true
}