## [Quest(ion) Engine](https://publish.obsidian.md/ddaemon/dentropydaemon-wiki/Projects/Quest(ion)+Engine)

## Requirements

* git
* nodejs + npm
* nvm
* metamask

## Install Instructions

**Run in your terminal**

``` bash
git clone https://gitlab.com/ddaemon/question-engine.git
cd question-engine
cd backend

npm  install -g nodemon
npm  install
nodemon index.js --reset
```

**Open a new terminal**

``` bash
cd frontend
npm run dev
```

**Load Test Data**

``` bash
cd question-engine/backend
npm run test
cat ./tests/persona_names.js
```

**Navigate to http://localhost:5173/ in your browser**

**Don't forget to install metamask**
