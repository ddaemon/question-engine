import * as React from 'react';
import Box from '@mui/material/Box';
import { ethers } from "ethers";
import Persona from '../classes/Persona';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import { useMetamask } from "../MetamaskProvider"
import Button from '@mui/material/Button';
const centerStyle = {
  position: 'relative',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  align: 'center',
  textAlign : 'center'
};

export default function MintMeme() {
  const [metamaskState, metamaskDispatch] = useMetamask();
  const [memeTitle, setMemeTitle] = React.useState(false);
  const [memeBody,  setMemeBody ] = React.useState(false);
  const [mintAmount, setMintAmount] = React.useState(1);
  async function mint_meme(){
    let MyPersona = new Persona("WEARING", metamaskState.account, metamaskState.pseudonym, "http://localhost:5173")
    MyPersona.setAccessToken(metamaskState.access_token)
    MyPersona.setPreviousTransaction(  await MyPersona.get_previous_transaction() )
    let meme_data = {
        "created": (new Date()).getTime(),
        "text": {
            title: memeTitle,
            body : memeBody
        },
        "type": "text",
        "meme_type_code": "ROOT_QUESTION",
        "permissions": "PUBLIC"
    }
    let transaction_data = await MyPersona.GENERATE_MEME_TRANSACTION(
      meme_data,
      metamaskState.pseudonym,  // token_issuer_pseudonym
      metamaskState.account,    // token_issuer_address
      mintAmount                // value
    )
    const eth_provider = new ethers.providers.Web3Provider(window.ethereum)
    const eth_signer = eth_provider.getSigner()
    transaction_data.signature_of_hash = await eth_signer.signMessage( transaction_data.hash )
    let transaction_result = await MyPersona.SEND_TRANSACTION(transaction_data)
    console.log(transaction_result)
    if(transaction_result.response_type == "SUCCESS"){
      metamaskDispatch({ type: "ACTIVE_MODAL", payload: false})
    }
  }
  return (
    <Box >
            <Typography 
              id="modal-modal-title" 
              variant="body1"
              style={centerStyle}>
              Mint a Meme
            </Typography>
            <br/>
            <TextField 
              style={centerStyle}
              type="text"
              onChange={() => setMemeTitle(event.target.value) } 
              id="MINT_MEME_TITLE"
              label="Meme Title"
              variant="outlined" />
            <br/><br/><br/>
            <TextField 
              style={centerStyle}
              type="number"
              defaultValue={mintAmount}
              onChange={() => setMintAmount(event.target.value) } 
              d="outlined-basic" 
              label="Number of Tokens" 
              variant="outlined" />
            <br/><br/><br/>
            <TextField 
              style={centerStyle}
              onChange={() => {
                // console.log(event.target.value)
                setMemeBody(event.target.value) 
              }}
              label="Meme Body"
              id="MINT_MEME_BODY" 
              multiline
              rows={4}
            />
            <br/>
            <Button 
              style={centerStyle}
              variant="contained" 
              onClick={() => { mint_meme() }}  
            >
              Mint Meme
            </Button>< br />
    </Box>
  );
}
