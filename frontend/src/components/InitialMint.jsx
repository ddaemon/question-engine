import * as React from 'react'
import Typography from '@mui/material/Typography'
import TextField from '@mui/material/TextField'
import { useMetamask } from "../MetamaskProvider"
import Button from '@mui/material/Button'
import Persona from "../classes/Persona"
import { ethers } from 'ethers'
export default function InitialMint() {
  const [metamaskState, metamaskDispatch] = useMetamask();
  const [mintAmount, setMintAmount] = React.useState(144);
  
  const centerStyle = {
    position: 'relative',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  };
  async function mint_tokens(){
    let MyPersona = new Persona("WEARING", metamaskState.account, metamaskState.pseudonym, "http://localhost:5173")
    MyPersona.setAccessToken(metamaskState.access_token)
    let transaction_data = await MyPersona.GENERATE_INITIAL_MINT_TRANSACTION(mintAmount)
    const eth_provider = new ethers.providers.Web3Provider(window.ethereum)
    const eth_signer = eth_provider.getSigner()
    transaction_data.signature_of_hash = await eth_signer.signMessage( transaction_data.hash )
    let transaction_result = await MyPersona.SEND_TRANSACTION(transaction_data)
    console.log(transaction_result)
    if(transaction_result.response_type == "SUCCESS"){
      metamaskDispatch({ type: "ACTIVE_MODAL", payload: false})
    }
  }
  return (
    <>
            <Typography id="initial mint modal inside" variant="body1">
            <b>TL;DR: Just click "Mint yourself 12 Tokens by clicking here" to get started on your first Q&A</b>
            <br /><br />
            Hi {metamaskState.pseudonym}, glad to see you using Quest(ion) Engine. We see your wallet of Pseudonym's QP's are empty. 
            No need for alarm, you can mint some yourself. 
            These tokens have your name on them because they are under your sovereign control.
            <a href="https://youtu.be/MRuS3dxKK9U?t=66">Your life has value after all.</a> After you have claimed your tokens you can use them to in Q&A's.
            <br /><br />
            Choose how many tokens you want to mint and select "STATE YOU EXIST" button below.
            <br /><br />
            </Typography>
            <br/>
            <br/>
            <TextField 
              style={centerStyle}
              type="number"
              defaultValue={mintAmount}
              onChange={() => setMintAmount(event.target.value) } 
              d="outlined-basic" 
              label="Number of Tokens" 
              variant="outlined" />
            <br/>
            <Button 
              style={centerStyle}
              onClick={() => mint_tokens()}
              variant="outlined">
                State You Exist
            </Button> <br />
            <Typography id="initial mint modal inside" variant="body1">
            P.S. QP stands for "Quest(ion) Point's" by the way.
            </Typography>

    </>
  );
}
