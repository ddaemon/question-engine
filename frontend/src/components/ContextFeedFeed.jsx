import * as React from 'react';

import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import MailIcon from '@mui/icons-material/Mail';
import Divider from '@mui/material/Divider';
import CustomCard from './Card';
import { Grid } from '@mui/material';
import Cookies from 'js-cookie';
import Card from '@mui/material/Card';

export default function ContextFeedFeed() {

  const [rows, setRows] = React.useState([
    { id: 1, FromPersona: 'Hello', Meme: 'World', MEME_PERMISSIONS : "PUBLIC",  MEME_TYPE_CODE: "Public", Timestamp: 10},
    { id: 2, FromPersona: 'DataGridPro', Meme: 'is Awesome', MEME_PERMISSIONS : "PUBLIC", MEME_TYPE_CODE: "Public",  Timestamp: 10 },
    { id: 3, FromPersona: 'MUI', Meme: 'is Amazing', MEME_PERMISSIONS : "PUBLIC",  MEME_TYPE_CODE: "Public", Timestamp: 10 },
  ]);

  React.useEffect(() => {
    // #TODO this does not work right now, also need to be integrated into RequireAuth
    async function updateChart(){
      let memes = await    fetch(
        '/api/query_memes',
        {
          method: 'POST', 
          body: JSON.stringify({
              query_type  : "PUBLIC_MEMES_CHRONOLOGICAL",
              access_token: Cookies.get('ACCESS_TOKEN')
          }),
          headers: {
              "content-type": "application/json"
          }
        }
      )
      memes = await memes.json()
      console.log(memes)
      let meme_rows = []
      if (memes.memes.length != 0 || memes != undefined){
        for(var i = 0; i < memes.memes.length; i++){
          meme_rows.push({
            id               : i,
            FromPersona      : memes.memes[i].AUTHOR_USER_ID,
            MEME_TITLE       : memes.memes[i].MEME_CONTENT.text.title,
            MEME_BODY        : memes.memes[i].MEME_CONTENT.text.body,
            MEME_TYPE_CODE   : memes.memes[i].MEME_TYPE_CODE,
            MEME_PERMISSIONS : memes.memes[i].MEME_PERMISSIONS,
            Timestamp        : memes.memes[i].updatedAt
          })
        }
      }
      setRows(meme_rows)
    }
    if ( Cookies.get('ACCESS_TOKEN') != undefined ) {
      updateChart()
    }
  }, [])
  function get_public_memes_reverse_chronological(){
    fetch(
      '/api/query_memes',
      {
        method: 'POST', 
        body: JSON.stringify({
            query_type  : "PUBLIC_MEMES_REVERSE_CHRONOLOGICAL",
            access_token: Cookies.get('ACCESS_TOKEN')
        }),
        headers: {
            "content-type": "application/json"
        }
      }
    ).then(res => res.json())
    .then(result => {
      console.log(result)
      return result
    })
  }
  
  function get_public_memes_chronological(){
    fetch(
      '/api/query_memes',
      {
        method: 'POST', 
        body: JSON.stringify({
            query_type  : "PUBLIC_MEMES_CHRONOLOGICAL",
            access_token: Cookies.get('ACCESS_TOKEN')
        }),
        headers: {
            "content-type": "application/json"
        }
      }
    ).then(res => res.json())
    .then(result => {
      console.log(result)
    })
  }
  return (
    <>
        <p>Feed</p>
        <List>
            {rows.map((item) => (
              <>
              <Card sx={{ minWidth: 275 }}>
                <Typography variant="h5" gutterBottom>
                  {item.MEME_TITLE}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                  {item.MEME_BODY}
                </Typography>
              </Card>
              <Divider />
              </>
            ))}
        </List>
    </>
  )
}
