import * as React from 'react'
import { useMetamask } from "../MetamaskProvider"
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import MintMeme from '../components/MintMeme'
import InitialMint from '../components/InitialMint'

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '80%',
  height: '80%',
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

export default function MetaModal(props) {
  const [metamaskState, metamaskDispatch] = useMetamask();
  const [open, setOpen] = React.useState(false);
  const MINT_MEME = () => {
    setOpen(true)
  }
  const handleClose = () => {
    metamaskDispatch({ type: "ACTIVE_MODAL", payload: false})
    setOpen(false)
  }

  const [initialOpen, setInitialOpen] = React.useState(false);
  const handleInitialOpen  = () => setInitialOpen(true);
  const handleInitialClose = () => {
    metamaskDispatch({ type: "ACTIVE_MODAL", payload: false})
    setInitialOpen(false)
  }
  
  React.useEffect(() => {
    if (metamaskState.active_modal == "INITIAL_MINT"){
      setInitialOpen(true)
    }
    if (metamaskState.active_modal == "MINT_MEME"){
      setOpen(true)
    }
    if (metamaskState.active_modal == false){
      setInitialOpen(false)
      setOpen(false)
    }
  },[metamaskState.active_modal])

  return (
    <>
      <Modal
          open={initialOpen}
          onClose={handleInitialClose}
          aria-labelledby="amodal-modal-title"
          aria-describedby="amodal-modal-description"
        > 
          <Box sx={style}>
            < InitialMint />
          </Box>
        </Modal>
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            < MintMeme />
          </Box>
        </Modal>
    </>
  );
}
