// src/Card.js
import React from 'react';
import { Card, CardContent, Typography } from '@mui/material';

const CustomCard = ({ title, content }) => {
  return (
    <Card>
      <CardContent>
        <Typography variant="h5" component="h2">
          {title}
        </Typography>
        <Typography color="textSecondary">{content}</Typography>
      </CardContent>
    </Card>
  );
};

export default CustomCard;