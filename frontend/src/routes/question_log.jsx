import * as React from 'react';
import Button from '@mui/material/Button';
import { Link } from "react-router-dom";
import PageSelect from '../components/PageSelect'

export default function question_log() {
  return (
    <>
        <PageSelect />
        <h1>Question Log!</h1>
        <p>You have no logs, check our your "Discovery" tab to interact with other personas or select the "Your Persona" tab and describe to the world who you are via questions and answers.</p>
        <Link to={"/"}> <Button variant="contained">Home</Button></Link>
    </>
  )
}
