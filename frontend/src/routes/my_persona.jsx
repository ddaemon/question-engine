import * as React from 'react';
import Button from '@mui/material/Button';
import PageSelect from '../components/PageSelect'
import { useMetamask } from "../MetamaskProvider"
import Typography from '@mui/material/Typography';
import MetaModal from '../components/MetaModal'

export default function my_persona() {
  // Mint a Meme Modal Stuff
  const [metamaskState, metamaskDispatch] = useMetamask();

  function updatePersona(){
    metamaskState.persona.setPreviousTransaction("PAUL WAS HERE")
  }
  return (
    <>
        <PageSelect />
        <Typography>My Persona</Typography>
        <Button onClick={() => {console.log(metamaskState.persona)}}>Print Persona</Button> <br />
        <Button onClick={updatePersona}>Update Persona</Button> <br />
        <Button onClick={() => {
          metamaskDispatch({ type: "ACTIVE_MODAL", payload: "MINT_MEME"})
          }}>
            Mint a Meme
        </Button> <br />
        <Button onClick={() => {
          metamaskDispatch({ type: "ACTIVE_MODAL", payload: "INITIAL_MINT"})
          }}>
          State You Exist
        </Button> <br />
        <MetaModal active_modal={metamaskState.active_modal}/>
    </>
  )
}
