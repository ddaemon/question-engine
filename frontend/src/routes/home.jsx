import * as React from 'react';
import Button from '@mui/material/Button';
import { Link } from "react-router-dom";
import Cookies from 'js-cookie';

export default function home() {
  function deleteCookies() {
    console.log( Cookies.remove('TOKEN') )
    console.log( Cookies.remove('PSEUDONYM') )
    console.log( Cookies.remove('ACCOUNT') )
  }
  function printCookies() {
    console.log( Cookies.get('TOKEN') )
    console.log( Cookies.get('PSEUDONYM') )
    console.log( Cookies.get('ACCOUNT') )
  }

  return (
    <>
        <h1>Quest(ion) Engine</h1>
        <h3><i>Discover who you are with the help of others</i></h3>
        <p>Discover who you are by wielding a persona, your identity is an illusion that you build and control. Articulate who you are with help from others trying to do the same. Select the "Wield Persona" button below to get started with Quest(ion) Engine.</p>
        <Link to={"/wield_persona"}> <Button variant="contained">Wield Persona</Button></Link>< br />
        <Button variant="contained" onClick={deleteCookies}>Delete Cookies</Button>
        <Button variant="contained" onClick={printCookies}>Print Cookies</Button>< br />
    </>
  )
}
