import * as React from 'react';
import Button from '@mui/material/Button';
import PageSelect from '../components/PageSelect'
import { DataGrid } from '@mui/x-data-grid';
import Cookies from 'js-cookie';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';



const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

export default function discovery() {
  const [rows, setRows] = React.useState([
    { id: 1, FromPersona: 'Hello', Meme: 'World', MEME_PERMISSIONS : "PUBLIC",  MEME_TYPE_CODE: "Public", Timestamp: 10},
    { id: 2, FromPersona: 'DataGridPro', Meme: 'is Awesome', MEME_PERMISSIONS : "PUBLIC", MEME_TYPE_CODE: "Public",  Timestamp: 10 },
    { id: 3, FromPersona: 'MUI', Meme: 'is Amazing', MEME_PERMISSIONS : "PUBLIC",  MEME_TYPE_CODE: "Public", Timestamp: 10 },
  ]);
  const [dataVIZ, setDataVIZ] = React.useState(
    <p>There are no public users yet, that doesn't mean there is nobody here though. Someone set this app up after all. Please go to the "Profile" tab add yourself a little description and start asking questions.</p>
  )
  const columns = [
    { field: 'FromPersona',       headerName: 'From', width: 150 },
    { field: 'MEME_TITLE',        headerName: 'Meme', width: 400 },
    { field: 'MEME_TYPE_CODE',    headerName: 'Meme Type', width: 150 },
    { field: 'MEME_PERMISSIONS',  headerName: 'Meme Permissions', width: 150 },
    { field: 'Timestamp',         headerName: 'Meme Timestamp', width: 150 },
  ];
  
  React.useEffect(() => {
    // #TODO this does not work right now, also need to be integrated into RequireAuth
    async function updateChart(){
      let memes = await    fetch(
        '/api/query_memes',
        {
          method: 'POST', 
          body: JSON.stringify({
              query_type  : "PUBLIC_MEMES_CHRONOLOGICAL",
              access_token: Cookies.get('ACCESS_TOKEN')
          }),
          headers: {
              "content-type": "application/json"
          }
        }
      )
      memes = await memes.json()
      console.log(memes)
      let meme_rows = []
      if (memes.memes.length != 0 || memes != undefined){
        for(var i = 0; i < memes.memes.length; i++){
          meme_rows.push({
            id               : i,
            FromPersona      : memes.memes[i].AUTHOR_USER_ID,
            MEME_TITLE       : memes.memes[i].MEME_CONTENT.text.title,
            MEME_TYPE_CODE   : memes.memes[i].MEME_TYPE_CODE,
            MEME_PERMISSIONS : memes.memes[i].MEME_PERMISSIONS,
            Timestamp        : memes.memes[i].updatedAt
          })
        }
      }
      setRows(meme_rows)
      setDataVIZ(
        <div style={{ height: 300, width: "100%" }}>
            <DataGrid rows={meme_rows} columns={columns} />
        </div>
      )



    }
    if ( Cookies.get('ACCESS_TOKEN') != undefined ) {
      updateChart()
    }
  }, [])
  function get_public_memes_reverse_chronological(){
    fetch(
      '/api/query_memes',
      {
        method: 'POST', 
        body: JSON.stringify({
            query_type  : "PUBLIC_MEMES_REVERSE_CHRONOLOGICAL",
            access_token: Cookies.get('ACCESS_TOKEN')
        }),
        headers: {
            "content-type": "application/json"
        }
      }
    ).then(res => res.json())
    .then(result => {
      console.log(result)
      return result
    })
  }
  
  function get_public_memes_chronological(){
    fetch(
      '/api/query_memes',
      {
        method: 'POST', 
        body: JSON.stringify({
            query_type  : "PUBLIC_MEMES_CHRONOLOGICAL",
            access_token: Cookies.get('ACCESS_TOKEN')
        }),
        headers: {
            "content-type": "application/json"
        }
      }
    ).then(res => res.json())
    .then(result => {
      console.log(result)
    })
  }

  return (
    <>
      <PageSelect />
      <Grid container spacing={12} style={ {width: window.innerWidth} } >
        <Grid item xs={2}>
          <Button onClick={get_public_memes_reverse_chronological} variant="contained">Get Public Memes Reverse Chronological</Button>
        </Grid>
        <Grid item xs={2}>
          <Button onClick={get_public_memes_chronological} variant="contained">TODO Get Public Memes Chronological</Button>
        </Grid>
        <Grid item xs={2}>
          <Button onClick={get_public_memes_reverse_chronological} variant="contained">TODO Query</Button>
        </Grid>
        <Grid item xs={2}>
          <Button onClick={get_public_memes_reverse_chronological} variant="contained">TODO Query</Button>
        </Grid>
        <Grid item xs={2}>
          <Button onClick={get_public_memes_reverse_chronological} variant="contained">TODO Query</Button>
        </Grid>
        <Grid item xs={2}>
          <Button onClick={get_public_memes_reverse_chronological} variant="contained">TODO Query</Button>
        </Grid>
        <Grid item xs={12}>
          {dataVIZ}
        </Grid>
      </Grid>
    </>
  )
}
