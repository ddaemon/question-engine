import { ethers } from "ethers";
import * as React from 'react';
import Button from '@mui/material/Button';
import { Navigate, useNavigate } from "react-router-dom";
import { TextField } from '@mui/material';
import { useMetamask } from "../MetamaskProvider"
import Cookies from 'js-cookie';
import Persona from "../classes/Persona";

export default function wield_persona() {
  const [metamaskState, metamaskDispatch] = useMetamask();
  const [ signer, setSigner ] = React.useState('')
  
  React.useEffect(() => {
    // #TODO this does not work right now, also need to be integrated into RequireAuth
    if ( Cookies.get('ACCESS_TOKEN') != undefined &&  Cookies.get('PSEUDONYM') != undefined && Cookies.get('ACCOUNT') != undefined) {
      fetch(
        '/api/check_access_token',
        {
          method: 'POST', 
          body: JSON.stringify({
              pseudonym   : Cookies.get('ACCOUNT'),
              evm_address : Cookies.get('PSEUDONYM'),
              access_token: Cookies.get('ACCESS_TOKEN')
          }),
          headers: {
              "content-type": "application/json"
          }
        }
      ).then(res => res.json())
      .then(result => {
        console.log(result)
        if (result.response_code == "VALID_ACCESS_TOKEN"){
          metamaskDispatch({ type: "ACCOUNT",      payload: Cookies.get('ACCOUNT')})
          metamaskDispatch({ type: "PSEUDONYM",    payload: Cookies.get('PSEUDONYM')})
          metamaskDispatch({ type: "ACCESS_TOKEN", payload: Cookies.get('ACCESS_TOKEN')})
          metamaskDispatch({ type: "SIGNED_IN",    payload: true})
          // metamaskDispatch({ type: "PERSONA",      payload: new Persona()})
        } else {
          console.log("Deleting those cookies")
          Cookies.remove('ACCOUNT')
          Cookies.remove('PSEUDONYM')
          Cookies.remove('ACCESS_TOKEN')
        }
      console.log("wield_persona useEffect")
      })

    }

    // Setup Listen Handlers on MetaMask change events
    if(typeof window.ethereum !== 'undefined') {
        // Add Listener when accounts switch
        window.ethereum.on('accountsChanged', (accounts) => {

          console.log('Account changed: ', accounts[0])
          // setWalletAccount(accounts[0])
          metamaskDispatch({ type: "ACCOUNT", payload: accounts[0]})
          metamaskDispatch({ type: "IS_CONNECTED", payload: true})
        })
        
        // Do something here when Chain changes
        window.ethereum.on('chainChanged', (chaindId) => {

          console.log('Chain ID changed: ', chaindId)
          setCurrentChain(chaindId)

        })

    } else {
        // metamaskDispatch({ type: "IS_CONNECTED", payload: false})
        alert('Please install MetaMask to use this service!')

    }
  }, [])

  function deleteCookies() {
    console.log( Cookies.remove('token') )
    console.log( Cookies.remove('pseudonym') )
    console.log( Cookies.remove('account') )
  }


  function printStuff() {
    console.log( Cookies.get('token') )
    console.log( Cookies.get('pseudonym') )
    console.log( Cookies.get('account') )
  }


  const handleConnectOnce = async () => {
      const provider = new ethers.providers.Web3Provider(window.ethereum)
      let accounts = await provider.send("eth_requestAccounts", []);
      metamaskDispatch({ type: "ACCOUNT", payload: accounts[0]})
      metamaskDispatch({ type: "IS_CONNECTED", payload: true})
      const signer = provider.getSigner()
      setSigner(signer)
      metamaskDispatch({ type: "SIGNER", payload: signer})
  }

  const DisplayWallet = () => {
    if (metamaskState.is_connected){
      return (<>
        <p>Your current wallet is {metamaskState.account}</p>
        <p>Persona Name: </p>
        {/* <TextField onChange={() => setPseudonym(event.target.value)} id="outlined-basic" label="Name of Persona" variant="outlined" /> */}
        <TextField onChange={() => metamaskDispatch({ type: "PSEUDONYM", payload: event.target.value}) } id="outlined-basic" label="Name of Persona" variant="outlined" />
        <Button variant="contained" onClick={handlePersonalSign}>Sign</Button>< br />
        <Button variant="contained" onClick={printStuff}>Print Cookies</Button>< br />
        <Button variant="contained" onClick={deleteCookies}>Delete Cookies</Button>
      </>)
    } else {
      return (
        <>
        <p>Please select Connect Metamask to get started</p>
        <Button onClick={handleConnectOnce}>Connect Metamask</Button>
        </>
      )
    }
  }

  const handlePersonalSign = () => {
    console.log('Sign Authentication')

    let data = {
      "pseudonym"    : metamaskState.pseudonym,
      "unix_time_ms" : (new Date()).getTime(),
      "signing_key"  : metamaskState.account
    }
    let data_string = JSON.stringify(data)
    let calculated_hash = ethers.utils.keccak256(ethers.utils.toUtf8Bytes(data_string))
    metamaskDispatch({ type: "CALCULATED_HASH", payload: calculated_hash})
    if(metamaskState.signer != null){
    metamaskState.signer.signMessage(calculated_hash)
    .then(signed_hash => {
      fetch(
          "/api/wield_persona", 
          {
              method: 'POST', 
              body: JSON.stringify({
                  data : data_string,
                  hash : calculated_hash,
                  signature_of_hash: signed_hash
              }),
              headers: {
                  "content-type": "application/json"
              }
          }
      ).then(res => res.json())
      .then(response_json => {
        console.log(response_json)
        if (response_json.response_type == "SUCCESS"){
          console.log(response_json)
          Cookies.set('PSEUDONYM',    metamaskState.pseudonym,    { expires: 7 });
          Cookies.set('ACCOUNT',      metamaskState.account,      { expires: 7 });
          Cookies.set('ACCESS_TOKEN', response_json.data.ACCESS_TOKEN, { expires: 7 });
          metamaskDispatch({ type: "ACCESS_TOKEN", payload: response_json.data.ACCESS_TOKEN})
          metamaskDispatch({ type: "SIGNED_IN",    payload: true})
          metamaskDispatch({ type: "PERSONA",      payload: new Persona(metamaskState.pseudonym, metamaskState.account, "")})
        }

      })
      
    })
    } else {
      console.log("Metamask Context Signer did not work")
    }
  }

  const RedirectOnceLoggedIn = () => {
    if (metamaskState.signed_in){
      return <Navigate to="/question_log" replace={true} />
    }
  }

  return (
    <>
        <h1>Wield Persona</h1>
        {DisplayWallet()} <br />
        {RedirectOnceLoggedIn()}
    </>
  )
}
