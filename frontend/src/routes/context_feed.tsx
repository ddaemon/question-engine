import * as React from 'react';
import Button from '@mui/material/Button';
import { Link } from "react-router-dom";
import PageSelect from '../components/PageSelect'
import { ContextFeedProvider, useContextFeed } from "../ContextFeedProvider"
import ContextFeedContext from '../components/ContextFeedContext';
import ContextFeedFeed from '../components/ContextFeedFeed';
import ContextFeedFilter from '../components/ContextFeedFilter';

import { styled, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import MailIcon from '@mui/icons-material/Mail';

const drawerWidth = 500;

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: 'flex-end',
}));

export default function context_feed() {
  const theme = useTheme();

  const [openFeed, setOpenFeed] = React.useState(false);
  const handleDrawerOpenFeed = () => {
    setOpenFeed(true);
  };

  const handleDrawerCloseFeed = () => {
    setOpenFeed(false);
  };

  const [openFilter, setOpenFilter] = React.useState(false);
  const handleDrawerOpenFilter = () => {
    setOpenFilter(true);
  };

  const handleDrawerCloseFilter = () => {
    setOpenFilter(false);
  };
  return (
    <>
        <PageSelect />
        <ContextFeedProvider>


        <CssBaseline />


        <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpenFeed}
            edge="start"
            sx={{ mr: 2, ...(openFeed && { display: 'none' }) }}
            style={{ position: 'absolute', left: '0' }}
          >
            <MenuIcon />
        </IconButton>

        <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpenFilter}
            edge="start"
            sx={{ mr: 2, ...(openFilter && { display: 'none' }) }}
            style={{ position: 'absolute', right: '0' }}
          >
            <MenuIcon />
        </IconButton>



        <Drawer
          sx={{
            width: drawerWidth,
            flexShrink: 0,
            '& .MuiDrawer-paper': {
              width: drawerWidth,
              boxSizing: 'border-box',
            },
          }}
          variant="persistent"
          anchor="left"
          open={openFeed}
        >
          <DrawerHeader>
            <IconButton 
              onClick={handleDrawerCloseFeed}
              style={{ position: 'absolute', right: '0' }}
            >
              {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
            </IconButton>
          </DrawerHeader>
          <ContextFeedFeed />
        </Drawer>


        <Drawer
          sx={{
            width: drawerWidth,
            flexShrink: 0,
            '& .MuiDrawer-paper': {
              width: drawerWidth,
              boxSizing: 'border-box',
            },
          }}
          variant="persistent"
          anchor="right"
          open={openFilter}
        >
          <DrawerHeader>
            <IconButton 
              onClick={handleDrawerCloseFilter}
              style={{ position: 'absolute', left: '0' }}
            >
              {theme.direction === 'rtl' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
            </IconButton>
          </DrawerHeader>
          <ContextFeedFilter />
        </Drawer>


          <ContextFeedContext />
          

        </ContextFeedProvider>
        <Link to={"/"}> <Button variant="contained">Home</Button></Link>
    </>
  )
}
