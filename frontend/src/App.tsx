import * as React from 'react'
import './App.css'

import {
  createBrowserRouter,
  RouterProvider,
  useLocation,
  Navigate,
  useNavigate
} from "react-router-dom";
import ErrorPage from './error-page';
import Home from './routes/home';
import WieldPersona from './routes/wield_persona'
import { MetamaskProvider, useMetamask } from "./MetamaskProvider"
import ContextFeed from "./routes/context_feed"
import QuestionLog from "./routes/question_log"
import MyPersona from "./routes/my_persona"
import Discovery from "./routes/discovery"
import MemeMapping from "./routes/meme_mapping"




const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
    errorElement: <ErrorPage />
  },
  {
    path: "/wield_persona",
    element: <WieldPersona />,
    errorElement: <ErrorPage />
  },
  {
    path: "/question_log",
    element: <RequireAuth><QuestionLog/></RequireAuth>,
    errorElement: <ErrorPage />
  },
  {
    path: "/my_persona",
    element: <RequireAuth><MyPersona/></RequireAuth>,
    errorElement: <ErrorPage />
  },
  {
    path: "/context_feed",
    element: <RequireAuth><ContextFeed/></RequireAuth>,
    errorElement: <ErrorPage />
  },
  {
    path: "/discovery",
    element: <RequireAuth><Discovery/></RequireAuth>,
    errorElement: <ErrorPage />
  },
  {
    path: "/meme_mapping",
    element: <RequireAuth><MemeMapping/></RequireAuth>,
    errorElement: <ErrorPage />
  },
  {
    path: "/persona/:personaId",
    element: <div>My Persona!</div>,
    errorElement: <ErrorPage />
  },
]);


function RequireAuth({ children }: { children: JSX.Element }) {
  let auth : any = useMetamask();
  let location = useLocation();
  console.log(auth)
  if (!auth[0].signed_in) {
    // Redirect them to the /login page, but save the current location they were
    // trying to go to when they were redirected. This allows us to send them
    // along to that page after they login, which is a nicer user experience
    // than dropping them off on the home page.
    return <Navigate to="/" state={{ from: location }} replace />;
  }

  return children;
}


function App() {
  // const [state, dispatch] = React.useContext(MetamaskContext);

  return (
    <MetamaskProvider>
      <RouterProvider router={router} />
    </MetamaskProvider>
  )
}

export default App
