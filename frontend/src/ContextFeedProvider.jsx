import * as React from 'react';

// [How to use React Context effectively](https://kentcdodds.com/blog/how-to-use-react-context-effectively)

export const ContextFeedContext = React.createContext();

function contextFeedReducer(state , action ) {
  console.log(state)
  switch (action.type) {
    case 'FEED_NONCE':
      return { ...state, feed_nonce: action.payload };
    case 'FILTER_QUERY':
      return { ...state, filter_query: action.payload };
    default:
      throw new Error(`Unhandled action type: ${action.type}`);
  }
}

  

export function ContextFeedProvider( props ) {
  const initialState = {
    "feed_nonce"      : false,
    "filter_query"    : "public",
  };  
  const [state, dispatch] = React.useReducer(contextFeedReducer, initialState);
  return (
    <ContextFeedContext.Provider value={[state, dispatch]}>
      {props.children}
    </ContextFeedContext.Provider>
  );
}

export function useContextFeed() {
  const context = React.useContext(ContextFeedContext)
  if (context === undefined) {
    throw new Error('useMetamask must be used within a ContextFeedProvider')
  }
  return context
}
