import * as React from 'react';

// [How to use React Context effectively](https://kentcdodds.com/blog/how-to-use-react-context-effectively)

export const MetamaskContext = React.createContext();

function metamaskReducer(state , action ) {
  console.log(state)
  switch (action.type) {
    case 'IS_CONNECTED':
      return { ...state, is_connected: action.payload };
    case 'PSEUDONYM':
      return { ...state, pseudonym: action.payload };
    case 'ACCOUNT':
      return { ...state, account: action.payload };
    case 'ACCESS_TOKEN':
      return { ...state, access_token: action.payload };
    case 'SIGNED_IN':
      return { ...state, signed_in: action.payload };
    case 'CALCULATED_HASH':
      return { ...state, calculated_hash: action.payload };
    case 'SIGNED_HASH':
      return { ...state, signed_hash: action.payload };
    case 'SIGNER':
      return { ...state, signer: action.payload };
    case 'PERSONA':
      return { ...state, persona: action.payload };
    case 'ACTIVE_MODAL':
      console.log(state)
      return { ...state, active_modal: action.payload };
    default:
      throw new Error(`Unhandled action type: ${action.type}`);
  }
}

  

export function MetamaskProvider( props ) {
  const initialState = {
    "is_connected"    : false,
    "account"         : null,
    "access_token"    : null,
    "signer"          : null,
    "persona"         : null,
    "pseudonym"       : "",
    "signed_in"       : false,
    "calculated_hash" : "",
    "signed_hash"     : "",
    "active_modal"    : false
  };  
  const [state, dispatch] = React.useReducer(metamaskReducer, initialState);
  return (
    <MetamaskContext.Provider value={[state, dispatch]}>
      {props.children}
    </MetamaskContext.Provider>
  );
}

export function useMetamask() {
  const context = React.useContext(MetamaskContext)
  if (context === undefined) {
    throw new Error('useMetamask must be used within a MetamaskProvider')
  }
  return context
}
